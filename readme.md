
## 目录

* [git工作流](/gitflow/gitflow.md)
* [maven简明指南](/maven/maven.md)
* [eclipse配置简介](/environment_create/environment_for_eclipse.md)
* [项目组工作日志](/teamLog/teamLog.md)
* [安装部署指南](/install/install.md)

## git工作流演习

首先，请了解什么是[git工作流](/gitflow/gitflow.md)

然后，按照下面的步骤做演习（只是建议）：
1. 先clone项目develop分支到本地
1. 按小组分配feature，feature数量和小组人数相同，每人一个feature：
  * 在本地建feature分支，增加、编辑、修改feature文档，提交到本地feature分支。分支命名：feature/{人名缩写}-{featureName}，如feature/zhangsan-feature1。
  * 使用Pull Requests合并：将feature分支提交到远程仓库，向组长发送merge Requests请求
  * 组长处理merge Requests请求，将feature分支合并到develop分支
1. 演习多人处理一个feature：还是按照上面的流程操作，只是有多人对同一个文件进行编辑操作，练习对冲突的处理


### ngschoolsite项目的git工作流

ngschoolsite用于各个中心的宣传、报名，一套代码，每个中心分别独立部署。每个中心有自己风格的页面，可以完全不同。因此，每个中心不同的是有自己的模板和静态资源。

* 每个中心作为一个大分支，以中心简称命名（和静态资源的目录名相同）。
* 一个中心由一个小组负责：
  * 组员可以单独负责或者多人联合开发页面，以feature分支的形式；
  * 组长负责合并feature分支进入中心大分支；大分支阶开发完成后，提交merge Requests合并到develop分支；
  * master负责合并develop分支，并建发布分支交由另外一组进行测试；测试通过后，合并回develop分支，并提交merge Requests给owner
  * owner负责合并到master分支，并打tag

注意：因为一套代码多处部署，因此不可以随意添加和修改后台代码，每个中心只能修改自己的静态资源。如果需要增加或修改后台代码，必须上报master和owner协商。


