# git工作流

源自：https://github.com/xirong/my-git/blob/master/git-workflow-tutorial.md

用中央仓库作为所有开发者的交互中心，开发者在本地工作并push分支到中央仓库



## 开发和发布历史分离

![git工作流](gitflow1.png)

### master分支
* 主分支，存储正式发布的历史，所有提交都要分配一个版本号
* 不允许直接往这个分支提交代码，只允许往这个分支发起merge request
* 只允许release分支和hotfix分支进行合流

### develop分支
* 开发分支，是功能的集成分支
* 用于日常开发，包括代码优化、功能性开发

## 功能分支/feature分支

![git工作流](gitflow2.png)

* 每个新功能位于一个自己的分支，分支命名规则：feature/姓名-功能名称，如feature/yd-feature1
* 从develop分支pull，完成后再合并回develop分支
* 从不直接与master分支交互

## 发布分支/release分支

![git工作流](gitflow3.png)

* 发布分支，一旦develop分支上有了做一次发布的足够功能，就从develop分支上fork一个发布分支。
* 这个新建的发布分支用于开始发布循环，因此从这个时间点开始，新的功能不能再加到这个分支上。这个分支只应该做Bug修复、文档生成和其它面向发布任务。
* 对外发布的工作都完成后，打tag并合入master和develop
* 分支命名: release/*

使用一个用于发布准备的专门分支，使得一个团队可以在完善当前的发布版本的同时，另一个团队可以继续开发下个版本的功能。
这也打造定义良好的开发阶段（比如，可以很轻松地说，『这周我们要做准备发布版本4.0』，并且在仓库的目录结构中可以实际看到）。


## 维护分支/hotfix分支

![git工作流](gitflow4.png)

* 维护/热更新分支，从master分支拉取，这是唯一可以直接从master分支fork出来的分支。
* 用于紧急修复上线版本的问题
* 修复后打tag并合入master和develop
* 分支命名: hotfix/*

## Pull Requests

开发者完成开发后，向上级或团队其他人发起通知，请求code review并合并到主分支

![Pull Requests](pull-requests-1.png)

团队其他成员review code，讨论并修改。项目维护者合并功能到官方仓库中并关闭Pull Request。

![Pull Requests](pull-requests-2.png)

Gitflow工作流，当一个功能、发布或是热修复分支需要Review时，开发者简单发起一个Pull Request，
团队的其它成员会通过Bitbucket收到通知。

新功能一般合并到develop分支，而发布和热修复则要同时合并到develop分支和master分支上。
Pull Request可能用做所有合并的正式管理。


## 总结

![gitflow full](gitflow-poolline.jpg)



## 实例代码


### 创建开发分支

1、为master分支配套一个develop分支。简单来做可以本地创建一个空的develop分支，push到服务器上：

```git
git branch develop
git push -u origin develop
```

2、其它开发者克隆中央仓库，建好develop分支的跟踪分支：

```git
git clone ssh://user@host/path/to/repo.git
git checkout -b develop origin/develop
```

### 开发新功能
在develop分支为新功能创建相应分支：
```git
git checkout -b feature/xxx-feature1 develop
```

然后按照老套路添加提交到这个功能分支上：编辑、暂存、提交
```git
git status
git add <some-file>
git commit
```

完成功能开发后，提交到develop分支：

（1）如果不使用Pull Requests，可以直接合并到本地develop分支后push到中央仓库
```git
git pull origin develop             // 确保develop分支是最新的
git checkout develop                // 合并到本地develop分支
git merge --no-ff feature/xxx-feature1              
git push                            // push到中央仓库
git branch -d feature/xxx-feature1          // 删除feature/xxx-feature1分支
```

冲突解决

在git pull origin develop时，可能发现有冲突导致失败，这时可以这样做：

```git
git pull --rebase origin develop     // 把提交移到同步了中央仓库修改后的develop分支的顶部
```

如果rebase过程中有冲突，会暂停并输出下面的信息：
```git
CONFLICT (content): Merge conflict in <some-file>
```

使用git status查看哪有问题，冲突文件列在Unmerged paths（未合并路径）一节中：
```git
# Unmerged paths:
# (use "git reset HEAD <some-file>..." to unstage)
# (use "git add/rm <some-file>..." as appropriate to mark resolution)
#
# both modified: <some-file>
```

编辑这些文件，暂存，然后继续git rebase:
```git
git add <some-file> 
git rebase --continue
```

所有冲突都做完了：
```git
git commit

git checkout develop                // 合并到本地develop分支
git merge --no-ff feature/xxx-feature1              
git push                            // push到中央仓库
git branch -d feature/xxx-feature1          // 删除feature/xxx-feature1分支
```


如果过程中出现问题，想回到执行git pull --rebase前的状态：
```git
git rebase --abort
```

（2）如果使用Pull Requests

开发者：

当一个功能、发布或是热修复分支需要Review时，开发者简单发起一个Pull Request，
团队的其它成员会通过Bitbucket收到通知：

提交新的功能分支到中央仓库
```git
git push -u origin feature/qq6
```

在gitlab上发起Merge request，其它成员会收到通知


合并负责人：

新功能一般合并到develop分支，而发布和热修复则要同时合并到develop分支和master分支上。
Pull Request可能用做所有合并的正式管理。

先拉回本地
```git
git fetch origin                                    //拉回本地
git checkout -b feature/qq6 origin/feature/qq6
```

在本地review代码

合并，并解除冲突
```git
git checkout develop
git merge --no-ff feature/qq6
```

提交合并后的结果到中央仓库
```git
git push origin develop

git branch -dr origin/feature/qq6   // 删除远程分支
git branch -d feature/qq6           // 删除本地分支
```


### 准备发布
```git
git checkout -b release/0.1 develop
```

在release/0.1分支下，做所有发布前的准备工作，并不再添加任何新功能

### 完成发布
一旦准备好了对外发布，合并修改到master分支和develop分支上，删除发布分支。

要求Code Review，这是一个发起Pull Request的理想时机

```git
git checkout master             //合并到master分支
git merge --no-ff release/0.1
git push

git tag -a 0.1 -m "Initial public release" master   //master分支打tag
git push --tags

git checkout develop        //合并到develop分支
git merge --no-ff release/0.1
git push
git branch -d release/0.1
```

可以配置一个勾子，在push中央仓库的master分支时，自动构建好对外发布。

### Bug修复

如果发布后发现了Bug，为了处理Bug，从master分支上拉出了一个维护分支，提交修改以解决问题，然后直接合并回master分支：

```git
git checkout -b hotfix/issue-#001 master
# Fix the bug
git checkout master
git merge --no-ff hotfix/issue-#001
git push

git tag -a 0.1 -m "Initial public release" master   //master分支打tag
git push --tags
```

同样的，这些重要修改需要包含到develop分支中，所以要执行一个合并操作。然后就可以安全地删除这个分支了：

```git
git checkout develop
git merge --no-ff hotfix/issue-#001
git push
git branch -d hotfix/issue-#001
```

