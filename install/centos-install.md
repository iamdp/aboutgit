# CentOS

# mysql
## install
```bsh
sudo yum -y install mariadb mariadb-server
sudo vim /etc/my.cnf

sudo systemctl start mariadb
sudo systemctl status mariadb
sudo systemctl enable mariadb
sudo mysql_secure_installation
```

my.cnf
```
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mariadb according to the
# instructions in http://fedoraproject.org/wiki/Systemd
character-set-server=utf8
bind-address = 127.0.0.1
lower_case_table_names = 1

[mysqld_safe]
log-error=/var/log/mariadb/mariadb.log
pid-file=/var/run/mariadb/mariadb.pid

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d

[client]
default-character-set=utf8

[mysql]
default-character-set=utf8
```

## create db
```
mysql -u root -p
create database new_neiguan character set utf8 collate utf8_general_ci;
use new_neiguan;
source new_neiguan_schema.sql;
source new_neiguan_data.sql;
call ng_test_init();
```

# redis
```
//wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
//rpm -ivh epel-release-latest-7.noarch.rpm
//yum repolist
yum search redis
yum install redis
systemctl status redis
systemctl enable redis
```

# JDK
```
rpm -qa | grep jdk
sudo rpm -qd jdk        //查看jdk安装路径：/usr/java/jdk1.8.0_144/

wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" \
"http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jdk-8u144-linux-x64.rpm" \
-O jdk-8u144-linux-x64.rpm

sudo rpm -ivh jdk-8u144-linux-x64.rpm

rpm -qa | grep jdk      //显示jdk1.8.0_144-1.8.0_144-fcs.x86_64
rpm -qd jdk1.8.0_144-1.8.0_144-fcs.x86_64    //查询安装位置

java -version
```

