# maven简单指南

本文档目的在于简单说明怎么安装和使用maven。

MAVEN，JAVA类项目的管理和构建工具。Apache开源项目，[http://maven.apache.org](http://maven.apache.org)。

## maven安装

示例是windows X64系统下的安装

### 安装JDK
* 下载JDK，[http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

![jdk download](jdk-download.png)

* 运行jdk-8u144-windows-x64.exe，安装JDK到一个目录，如c:\java\jdk\jdk1.8.0_144

* 配置系统环境变量(WinKey + Pause):JAVA_HOME，并把%JAVA_HOME%\bin加到path变量

![jdk java home](jdk-javahome.png)

* 验证是否配置成功：
在shell命令行(Winkey + R，输入cmd) ，任意一个目录下运行：java -version，都可以看到java版本信息，就表示jdk安装配置成功
```
java -version
```
![java version](jdk-version.png)

### 安装maven
* 下载maven：[http://maven.apache.org/download.cgi](http://maven.apache.org/download.cgi)

![maven download](mvn-download.png)

* 解压到一个目录，如c:\java\maven
* 把bin目录加到系统变量path中：c:\java\maven\bin
* 验证：在shell命令行，任意一个目录下运行：mvn -v，看到maven版本信息就表示成功
* 配置maven：修改maven目录下的conf\settings.xml，如c:\java\maven\conf\settings.xml

修改本地仓库的位置（也可以不改）：
```
  <localRepository>c:\java\repo</localRepository>
```
   * 从远程仓库拉下来的jar包都会存在这里，下次就不用再拉了。
   * 本地项目生成的jar包，也会放到这里，以便其它项目使用

增加国内镜像仓库：
```
 <mirrors>
     <mirror>
       <id>alimaven</id>
       <name>aliyun maven</name>
       <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
       <mirrorOf>central</mirrorOf>        
     </mirror>
  </mirrors>  
```


## maven项目文件

项目核心文件是pom.xml

常用命令：
```
mvn clean
mvn install
mvn test
mvn clean install
```

## 示例：命令行使用mvn编译运行ngschoolsite

### 先打包依赖项目，并发布到本地maven仓库

进入shell命令行，转到ngschoolservice-sdk-java目录：
```
cd ngschoolservice-sdk-java
mvn clean install
```

![mvn-clean-install.png](mvn-clean-install.png)

打包后的jar包复制到本地仓库了：

![mvn-clean-install-2.png](mvn-clean-install-2.png)

如果是第一次运行，因为要从远程仓库拉jar包，可能会比较慢。喝杯茶，慢慢等:-)

### 编译运行ngschoolsite

同样的，进入shell命令行，转到ngschoolsite目录：
```
cd ngschoolsite
mvn clean install
```

构建成功，可以看到sucess提示：

![mvn-clean-install-3.png](mvn-clean-install-3.png)

运行spring boot项目：
```
mvn spring-boot:run
```

出现spring boot图案，就表示WEB程序已经运行起来了：
![mvn-spring-boot-run.png](mvn-spring-boot-run.png)

在浏览器中输入：127.0.0.1:8090
![mvn-spring-boot-run2.png](mvn-spring-boot-run2.png)

可以看到shell窗口中有输出信息，表示调用api成功：
![mvn-spring-boot-run1.png](mvn-spring-boot-run1.png)

在命令窗口，按Ctrl+c终止程序执行

### 如何修改ngschoolsite页面模板
每个中心都可以有自己的模板和前端静态资源如JS、CSS等，放在以中心简称命名的目录下：

 *  静态资源：scr/main/resources/static/{school-name}
 *  模板：scr/main/resources/templates/{school-name}

在application.properties配置文件中指定运行时使用哪种中心的模板：

```
spring.resources.static-locations = classpath:/static/{school-name}/
spring.thymeleaf.prefix = classpath:/templates/{school-name}/
```

**注意：** templates目录下的**模板文件，名称要保持不变！**

示例：南禅寺内观中心，简称：nanchansi

只修改模板，静态资源先不管：

 *  模板：scr/main/resources/templates/nanchansi
 
在src\main\resources\templates目录下创建nanchansi目录（假定当前在shell命令行，当前目录是ngschoolsite）：
```
cd src\main\resources\templates
mkdir nanchansi
```

重新定义首页文件：
* 在nanchansi目录新建或复制index.html文件
* 编辑index.html文件


修改配置文件，ngschoolsite\src\main\resources\application.properties：
```
spring.thymeleaf.prefix = classpath:/templates/nanchansi
```

回到ngschoolsite目录，测试运行
```
cd ngschoolsite
mvn clean install
mvn spring-boot:run
```

在浏览器中输入：127.0.0.1:8090，查看修改后的效果

