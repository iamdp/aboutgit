-- MySQL dump 10.14  Distrib 5.5.52-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: new_neiguan
-- ------------------------------------------------------
-- Server version	5.5.52-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `article_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文章ID',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updator` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `title_image` varchar(250) DEFAULT NULL COMMENT '标题图片',
  `content` text COMMENT '内容',
  `can_publish` tinyint(1) DEFAULT NULL COMMENT '是否公开',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='文章';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `article_tag`
--

DROP TABLE IF EXISTS `article_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_tag` (
  `tag_id` bigint(20) NOT NULL COMMENT '标签ID',
  `article_id` bigint(20) NOT NULL COMMENT '文章ID',
  PRIMARY KEY (`tag_id`,`article_id`),
  KEY `FK_article_tag_article` (`article_id`),
  CONSTRAINT `FK_article_tag_article` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`),
  CONSTRAINT `FK_article_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `at`
--

DROP TABLE IF EXISTS `at`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `at` (
  `at_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '助理老师ID',
  `cn_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `en_name` varchar(50) DEFAULT NULL COMMENT '英文名',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别',
  `remark` text COMMENT '备注',
  PRIMARY KEY (`at_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='助理老师';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bad_enroll`
--

DROP TABLE IF EXISTS `bad_enroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bad_enroll` (
  `bad_enroll_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '不良记录ID',
  `course_id` bigint(20) DEFAULT NULL COMMENT '课程ID',
  `enroll_id` bigint(20) DEFAULT NULL COMMENT '报名ID',
  `schoolId` bigint(20) DEFAULT NULL COMMENT '中心ID',
  `school_name` varchar(100) DEFAULT NULL COMMENT '中心名称',
  `school_country` varchar(50) DEFAULT NULL COMMENT '中心所在国家',
  `course_date_from` date DEFAULT NULL COMMENT '课程开始时间',
  `course_date_to` date DEFAULT NULL COMMENT '课程结束时间',
  `course_type_name` varchar(50) DEFAULT NULL COMMENT '课程类型',
  `course_days` int(11) DEFAULT NULL COMMENT '课程天数',
  `course_class` varchar(50) DEFAULT NULL COMMENT '课程种类',
  `teacher` varchar(250) DEFAULT NULL COMMENT '带课老师',
  `cn_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `en_name` varchar(50) DEFAULT NULL COMMENT '英文名',
  `id_type` varchar(50) DEFAULT NULL COMMENT '证件类型',
  `id_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `isOld` tinyint(1) DEFAULT NULL COMMENT '是否旧生',
  `release_date` date DEFAULT NULL COMMENT '解禁日期',
  `remark` text COMMENT '备注',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updator` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`bad_enroll_id`),
  KEY `cn_name` (`cn_name`),
  KEY `id_num` (`id_num`),
  KEY `release_date` (`release_date`),
  KEY `FK_bad_enroll_course` (`course_id`),
  KEY `FK_bad_enroll_enroll` (`enroll_id`),
  CONSTRAINT `FK_bad_enroll_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
  CONSTRAINT `FK_bad_enroll_enroll` FOREIGN KEY (`enroll_id`) REFERENCES `enroll` (`enroll_id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=utf8 COMMENT='不良记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `code`
--

DROP TABLE IF EXISTS `code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code` (
  `code_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '参考记录ID',
  `study_work_record_id` bigint(20) DEFAULT NULL COMMENT '上课服务记录ID',
  `course_id` bigint(20) DEFAULT NULL COMMENT '课程ID',
  `schoolId` bigint(20) DEFAULT NULL COMMENT '中心ID',
  `school_name` varchar(100) DEFAULT NULL COMMENT '中心名称',
  `school_country` varchar(50) DEFAULT NULL COMMENT '中心所在国家',
  `course_date_from` date DEFAULT NULL COMMENT '课程开始时间',
  `course_date_to` date DEFAULT NULL COMMENT '课程结束时间',
  `course_type_name` varchar(50) DEFAULT NULL COMMENT '课程类型',
  `course_days` int(11) DEFAULT NULL COMMENT '课程天数',
  `course_class` varchar(50) DEFAULT NULL COMMENT '课程种类',
  `teacher` varchar(250) DEFAULT NULL COMMENT '带课老师',
  `cn_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `en_name` varchar(50) DEFAULT NULL COMMENT '英文名',
  `id_type` varchar(50) DEFAULT NULL COMMENT '证件类型',
  `id_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `student_type` varchar(50) NOT NULL COMMENT '学员类别',
  `code` varchar(50) DEFAULT NULL COMMENT 'CODE',
  `page` varchar(50) DEFAULT NULL COMMENT 'Page',
  `vnl` text COMMENT 'VNL',
  `vnl_photo` varchar(250) DEFAULT NULL COMMENT 'VNL照片',
  `release_date` date DEFAULT NULL COMMENT '解禁日期',
  `remark` text COMMENT '备注',
  `term_count` int(11) DEFAULT NULL COMMENT '期数',
  `advice_teacher` varchar(250) DEFAULT NULL COMMENT '提议老师',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updator` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`code_id`),
  KEY `cn_name` (`cn_name`),
  KEY `id_num` (`id_num`),
  KEY `release_date` (`release_date`),
  KEY `FK_code_course` (`course_id`),
  KEY `FK_code_study_work_record` (`study_work_record_id`),
  CONSTRAINT `FK_code_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`),
  CONSTRAINT `FK_code_study_work_record` FOREIGN KEY (`study_work_record_id`) REFERENCES `study_work_record` (`study_work_record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参考记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `course_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '课程ID',
  `course_type_id` bigint(20) NOT NULL COMMENT '课程类型ID',
  `school_id` bigint(20) NOT NULL COMMENT '中心ID',
  `enroll_date_from` date DEFAULT NULL COMMENT '报名开始时间',
  `enroll_date_to` date DEFAULT NULL COMMENT '报名结束时间',
  `enroll_confirm_date_from` date DEFAULT NULL COMMENT '报名确认开始时间',
  `enroll_confirm_date_to` date DEFAULT NULL COMMENT '报名确认结束时间',
  `arrival_confirm_date_from` date DEFAULT NULL COMMENT '行程确认开始时间',
  `arrival_confirm_date_to` date DEFAULT NULL COMMENT '行程确认结束时间',
  `male_new` int(11) DEFAULT '0' COMMENT '男众新生数量',
  `male_new_spare` int(11) DEFAULT '0' COMMENT '男众新生候补数量',
  `male_old` int(11) DEFAULT '0' COMMENT '男众旧生数量',
  `male_old_spare` int(11) DEFAULT '0' COMMENT '男众旧生候补数量',
  `female_new` int(11) DEFAULT '0' COMMENT '女众新生数量',
  `female_new_spare` int(11) DEFAULT '0' COMMENT '女众新生候补数量',
  `female_old` int(11) DEFAULT '0' COMMENT '女众旧生数量',
  `female_old_spare` int(11) DEFAULT '0' COMMENT '女众旧生候补数量',
  `male_worker` int(11) DEFAULT '0' COMMENT '男法工数量',
  `female_worker` int(11) DEFAULT '0' COMMENT '女法工数量',
  `course_code` varchar(50) DEFAULT NULL COMMENT '课程代码',
  `course_desp` text COMMENT '课程说明',
  `remark` text COMMENT '备注',
  `is_enroll_stoped` tinyint(1) DEFAULT NULL COMMENT '是否已停止报名',
  `course_date_from` date DEFAULT NULL COMMENT '课程开始时间',
  `course_date_to` date DEFAULT NULL COMMENT '课程结束时间',
  `language` varchar(50) DEFAULT NULL COMMENT '语言',
  `teachers` varchar(250) DEFAULT NULL COMMENT '带课老师',
  `admin` varchar(100) DEFAULT NULL COMMENT '管理老师',
  `status` varchar(20) DEFAULT NULL COMMENT '状态',
  `cancelcause` text COMMENT '课程停止原因',
  PRIMARY KEY (`course_id`),
  KEY `course_date_from` (`course_date_from`),
  KEY `course_date_to` (`course_date_to`),
  KEY `enroll_date_from` (`enroll_date_from`),
  KEY `enroll_date_to` (`enroll_date_to`),
  KEY `FK_course_school` (`school_id`),
  KEY `FK_course_course_type` (`course_type_id`),
  CONSTRAINT `FK_course_course_type` FOREIGN KEY (`course_type_id`) REFERENCES `course_type` (`course_type_id`),
  CONSTRAINT `FK_course_school` FOREIGN KEY (`school_id`) REFERENCES `school` (`school_id`)
) ENGINE=InnoDB AUTO_INCREMENT=942 DEFAULT CHARSET=utf8 COMMENT='课程';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course_type`
--

DROP TABLE IF EXISTS `course_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_type` (
  `course_type_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '课程类型ID',
  `course_type_name` varchar(50) DEFAULT NULL COMMENT '课程类型',
  `course_class` varchar(50) DEFAULT NULL COMMENT '课程种类',
  `course_days` int(11) DEFAULT NULL COMMENT '课程天数',
  `enroll_day` int(11) DEFAULT NULL COMMENT '报名日',
  `enroll_days` int(11) DEFAULT NULL COMMENT '报名天数',
  `first_comfirm_day` int(11) DEFAULT NULL COMMENT '一次确认日',
  `first_comfirm_days` int(11) DEFAULT NULL COMMENT '一次确认天数',
  `second_comfirm_day` int(11) DEFAULT NULL COMMENT '二次确认日',
  `second_comfirm_days` int(11) DEFAULT NULL COMMENT '二次确认天数',
  `student_limit_desp` text COMMENT '招生条件说明',
  `course_type_order` int(11) DEFAULT NULL COMMENT '课程类型序号',
  `student_limit` text COMMENT '招生条件',
  PRIMARY KEY (`course_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='课程类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course_type_template`
--

DROP TABLE IF EXISTS `course_type_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course_type_template` (
  `course_type_template_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '课程类型模板ID',
  `course_type_id` bigint(20) DEFAULT NULL COMMENT '课程类型ID',
  `message_template_id` bigint(20) DEFAULT NULL COMMENT '消息模板ID',
  `event_type` varchar(50) DEFAULT NULL COMMENT '事件类型',
  `action` varchar(100) DEFAULT NULL COMMENT '动作',
  `message_post_channels` varchar(100) DEFAULT NULL COMMENT '消息发送方式',
  `timer_baseday` varchar(50) DEFAULT NULL COMMENT '日期原点',
  `timer_days` int(11) DEFAULT NULL COMMENT '相对天数',
  `timer_hour` int(11) DEFAULT NULL COMMENT '启动时点小时',
  `timer_min` int(11) DEFAULT NULL COMMENT '启动时点分钟',
  `target` text COMMENT '目标对象',
  PRIMARY KEY (`course_type_template_id`),
  KEY `course_type_id_FK` (`course_type_id`),
  KEY `message_template_id_FK` (`message_template_id`),
  CONSTRAINT `FK_course_type_template_course_type` FOREIGN KEY (`course_type_id`) REFERENCES `course_type` (`course_type_id`),
  CONSTRAINT `FK_course_type_template_message_template` FOREIGN KEY (`message_template_id`) REFERENCES `message_template` (`message_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='课程类型模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `enroll`
--

DROP TABLE IF EXISTS `enroll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enroll` (
  `enroll_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '报名ID',
  `course_id` bigint(20) DEFAULT NULL COMMENT '课程ID',
  `recommendation` varchar(50) DEFAULT NULL COMMENT '推荐',
  `enroll_time` datetime DEFAULT NULL COMMENT '报名时间',
  `student_type` varchar(50) DEFAULT NULL COMMENT '学员类别',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别',
  `cn_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `en_name` varchar(50) DEFAULT NULL COMMENT '英文名',
  `id_type` varchar(50) DEFAULT NULL COMMENT '证件类型',
  `id_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `idcity` varchar(100) DEFAULT NULL COMMENT '身份证行政区划',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `mobile2` varchar(50) DEFAULT NULL COMMENT '紧急联系电话',
  `email2` varchar(50) DEFAULT NULL COMMENT '备用email',
  `photo` varchar(250) DEFAULT NULL COMMENT '照片',
  `address` varchar(250) DEFAULT NULL COMMENT '地址',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `postcode` varchar(10) DEFAULT NULL COMMENT '邮编',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `religion` varchar(50) DEFAULT NULL COMMENT '身份-宗教信仰',
  `religion_date` date DEFAULT NULL COMMENT '受戒日期',
  `profession` varchar(50) DEFAULT NULL COMMENT '职业',
  `occupation` varchar(50) DEFAULT NULL COMMENT '职位',
  `education` varchar(50) DEFAULT NULL COMMENT '学历',
  `parent` varchar(50) DEFAULT NULL COMMENT '家长',
  `parent_phone` varchar(50) DEFAULT NULL COMMENT '家长电话',
  `canEnglish` tinyint(1) DEFAULT NULL COMMENT '懂英文',
  `canChinese` tinyint(1) DEFAULT NULL COMMENT '普通话',
  `canTaiyu` tinyint(1) DEFAULT NULL COMMENT '闽南语',
  `canYueyu` tinyint(1) DEFAULT NULL COMMENT '广东话',
  `otherlanguages` varchar(100) DEFAULT NULL COMMENT '其他语言',
  `has_together` tinyint(1) DEFAULT NULL COMMENT '有同行人吗',
  `together` varchar(50) DEFAULT NULL COMMENT '同行人',
  `together_relationship` varchar(50) DEFAULT NULL COMMENT '与同行人的关系',
  `howKnown` varchar(1024) DEFAULT NULL COMMENT '如何得知内观',
  `had_study_other` tinyint(1) DEFAULT NULL COMMENT '有学习其它方法',
  `study_other` text COMMENT '其它方法',
  `had_teach` tinyint(1) DEFAULT NULL COMMENT '有教人',
  `teach` text COMMENT '教人',
  `had_physiclal` tinyint(1) DEFAULT NULL COMMENT '有身体状况',
  `physiclal` text COMMENT '身体状况',
  `had_mental` tinyint(1) DEFAULT NULL COMMENT '有心理状况',
  `mental` text COMMENT '心理状况',
  `had_medicine` tinyint(1) DEFAULT NULL COMMENT '有服药情况',
  `medicine` text COMMENT '服药情况',
  `had_drug` tinyint(1) DEFAULT NULL COMMENT '有成瘾情况',
  `drug` text COMMENT '成瘾情况',
  `isOld` tinyint(1) DEFAULT NULL COMMENT '是否旧生',
  `hasExercise` tinyint(1) DEFAULT NULL COMMENT '有持续练习',
  `hours_per_week` varchar(250) DEFAULT NULL COMMENT '每周练习小时数',
  `canEarlyHelp` tinyint(1) DEFAULT NULL COMMENT '可早到',
  `canWork` tinyint(1) DEFAULT NULL COMMENT '可做法工',
  `didPurser` varchar(50) DEFAULT NULL COMMENT '担任过事务长',
  `first_study_date_from` date DEFAULT NULL COMMENT '第一次课程时间',
  `first_study_school` varchar(250) DEFAULT NULL COMMENT '第一次课程地点',
  `first_study_teacher` varchar(250) DEFAULT NULL COMMENT '第一次课程老师',
  `last_study_date_from` date DEFAULT NULL COMMENT '最后一次课程时间',
  `last_study_school` varchar(250) DEFAULT NULL COMMENT '最后一次课程地点',
  `last_study_teacher` varchar(250) DEFAULT NULL COMMENT '最后一次课程老师',
  `study_count_10day` int(11) DEFAULT NULL COMMENT '参加十日课程次数',
  `study_count_SatipatthanaSutta` int(11) DEFAULT NULL COMMENT '参加四念住课程次数',
  `study_count_20day` int(11) DEFAULT NULL COMMENT '参加20日课程次数',
  `study_count_30day` int(11) DEFAULT NULL COMMENT '参加30日课程次数',
  `study_count_45day` int(11) DEFAULT NULL COMMENT '参加45日课程次数',
  `service_count_10day` int(11) DEFAULT NULL COMMENT '服务十日课程次数',
  `service_count_SatipatthanaSutta` int(11) DEFAULT NULL COMMENT '服务四念住课程次数',
  `service_count_20day` int(11) DEFAULT NULL COMMENT '服务20日课程次数',
  `service_count_30day` int(11) DEFAULT NULL COMMENT '服务30日课程次数',
  `service_count_45day` int(11) DEFAULT NULL COMMENT '服务45日课程次数',
  `heath_had_bloodPressure` tinyint(1) DEFAULT NULL COMMENT '健康声明_有血压',
  `heath_bloodPressure` text COMMENT '健康声明_血压',
  `heath_had_diabetes` tinyint(1) DEFAULT NULL COMMENT '健康声明_有糖尿病',
  `heath_diabetes` text COMMENT '健康声明_糖尿病',
  `heath_had_heart` tinyint(1) DEFAULT NULL COMMENT '健康声明_有心脏病',
  `heath_heart` text COMMENT '健康声明_心脏病',
  `heath_had_mental` tinyint(1) DEFAULT NULL COMMENT '健康声明_有精神',
  `heath_mental` text COMMENT '健康声明_精神',
  `heath_had_drug` tinyint(1) DEFAULT NULL COMMENT '健康声明_有成瘾',
  `heath_drug` text COMMENT '健康声明_成瘾',
  `heath_had_hallucination` tinyint(1) DEFAULT NULL COMMENT '健康声明_有幻觉',
  `heath_hallucination` text COMMENT '健康声明_幻觉',
  `heath_had_otherDisease` tinyint(1) DEFAULT NULL COMMENT '健康声明_有其它疾病',
  `heath_otherDisease` text COMMENT '健康声明_其它疾病',
  `heath_had_pregnancy` tinyint(1) DEFAULT NULL COMMENT '健康声明_有怀孕',
  `heath_pregnancy` text COMMENT '健康声明_怀孕',
  `heath_had_specialArrangements` tinyint(1) DEFAULT NULL COMMENT '健康声明_有特别安排',
  `heath_specialArrangements` text COMMENT '健康声明_特别安排',
  `linealKin` varchar(50) DEFAULT NULL COMMENT '直系亲属',
  `linealKin_phone` varchar(50) DEFAULT NULL COMMENT '直系亲属电话',
  `linealKin_relationship` varchar(50) DEFAULT NULL COMMENT '与申请人的关系',
  `studentOrWorker` varchar(10) DEFAULT NULL COMMENT '学员还是法工',
  `worker_work` varchar(250) DEFAULT NULL COMMENT '法工认领岗位',
  `worker_schools` varchar(1024) DEFAULT NULL COMMENT '法工可服务中心',
  `full_time` tinyint(1) DEFAULT NULL COMMENT '是否全程',
  `to_reach_date` varchar(250) DEFAULT NULL COMMENT '兼程到达时间',
  `to_leave_date` varchar(250) DEFAULT NULL COMMENT '兼程离开时间',
  `commitment_image` varchar(250) DEFAULT NULL COMMENT '承诺书图片文件',
  `healthDetail_image` varchar(250) DEFAULT NULL COMMENT '健康状况声明图片文件',
  `linealKin_id_image_front` varchar(250) DEFAULT NULL COMMENT '直系亲属身份证正面图片文件',
  `linealKin_id_image_back` varchar(250) DEFAULT NULL COMMENT '直系亲属身份证背面图片文件',
  `linealKin_video` varchar(250) DEFAULT NULL COMMENT '直系亲属手持身份证口头同意录像',
  `need_additional` tinyint(1) DEFAULT NULL COMMENT '是否需要上传补充资料',
  `first_confirm_time` datetime DEFAULT NULL COMMENT '一次确认时间',
  `first_confirm_user` varchar(50) DEFAULT NULL COMMENT '一次确认人员',
  `first_confirm_remark` text COMMENT '一次确认备注',
  `second_confirm_time` datetime DEFAULT NULL COMMENT '二次确认时间',
  `second_confirm_user` varchar(50) DEFAULT NULL COMMENT '二次确认人员',
  `second_confirm_remark` text COMMENT '二次确认备注',
  `second_confirm_arrivaltime` varchar(100) DEFAULT NULL COMMENT '预计到达时间',
  `checkin_remark` text COMMENT '报到日事项',
  `enroll_status` varchar(20) DEFAULT NULL COMMENT '报名状态',
  `course_finished` tinyint(1) DEFAULT NULL COMMENT '是否完成课程',
  `undone_course_reason` text COMMENT '未完成课程的原因',
  `remark` text COMMENT '备注',
  `check_status` varchar(20) DEFAULT NULL COMMENT '审核状态',
  `check_remark` text COMMENT '审核备注',
  `applicant_operations` text COMMENT '申请人允许的操作',
  `check_user_name` varchar(50) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL COMMENT '审核时间',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updator` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`enroll_id`),
  KEY `cn_name` (`cn_name`),
  KEY `id_num` (`id_num`),
  KEY `student_worker` (`studentOrWorker`),
  KEY `enroll_status` (`enroll_status`),
  KEY `enroll_check_status` (`check_status`),
  KEY `FK_enroll_course` (`course_id`),
  CONSTRAINT `FK_enroll_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=196593 DEFAULT CHARSET=utf8 COMMENT='报名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `enroll_check_cause_template`
--

DROP TABLE IF EXISTS `enroll_check_cause_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enroll_check_cause_template` (
  `enroll_check_cause_template_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '审核理由描述模板Id',
  `enroll_check_operation_type` varchar(50) DEFAULT NULL COMMENT '适用的审核操作',
  `cause_name` varchar(255) DEFAULT NULL COMMENT '理由名称',
  `case_description` text COMMENT '理由描述',
  `checkin_item` varchar(250) DEFAULT NULL COMMENT '报到日特殊事项',
  `applicant_operation_type` varchar(50) DEFAULT NULL COMMENT '允许的申请人操作类型',
  PRIMARY KEY (`enroll_check_cause_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='报告审核理由描述模板';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `enroll_operation_record`
--

DROP TABLE IF EXISTS `enroll_operation_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enroll_operation_record` (
  `enroll_operation_record_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '报名操作记录ID',
  `enroll_id` bigint(20) DEFAULT NULL COMMENT '报名ID',
  `operation` varchar(50) DEFAULT NULL COMMENT '操作',
  `old_check_status` varchar(20) DEFAULT NULL COMMENT '之前的审核状态',
  `check_status` varchar(20) DEFAULT NULL COMMENT '审核状态',
  `cause` varchar(200) DEFAULT NULL COMMENT '审核缘由分类',
  `cause_description` text COMMENT '审核缘由描述',
  `checkin_remark` text COMMENT '报到日事项',
  `applicant_operations` text COMMENT '申请人允许的操作',
  `check_remark` text COMMENT '审核备注',
  `operator_name` varchar(50) DEFAULT NULL COMMENT '操作者姓名',
  `operator_user_id` bigint(20) DEFAULT NULL COMMENT '操作者ID',
  `operate_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`enroll_operation_record_id`),
  KEY `Relationship_11_FK` (`enroll_id`),
  KEY `Relationship_13_FK` (`operator_user_id`),
  CONSTRAINT `FK_enroll_operation_record_enroll` FOREIGN KEY (`enroll_id`) REFERENCES `enroll` (`enroll_id`),
  CONSTRAINT `FK_enroll_operation_record_sys_user` FOREIGN KEY (`operator_user_id`) REFERENCES `sys_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='报名操作记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `file`
--

DROP TABLE IF EXISTS `file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file` (
  `file_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `file_original_name` varchar(100) DEFAULT NULL COMMENT '文件原名称',
  `file_mimetype` varchar(50) DEFAULT NULL COMMENT 'MIME类型',
  `file_full_path_name` varchar(250) DEFAULT NULL COMMENT '文件含路径全名',
  `file_usefor` varchar(50) DEFAULT NULL COMMENT '文件用途',
  `file_desp` varchar(250) DEFAULT NULL COMMENT '文件描述',
  `file_upload_time` datetime DEFAULT NULL COMMENT '文件上传时间',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `idnum_city`
--

DROP TABLE IF EXISTS `idnum_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `idnum_city` (
  `idnum_city_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '身份证所属城市ID',
  `province_id` varchar(50) DEFAULT NULL COMMENT '省ID',
  `city_id` varchar(50) DEFAULT NULL COMMENT '市ID',
  `zone_id` varchar(50) DEFAULT NULL COMMENT '区ID',
  `province_name` varchar(50) DEFAULT NULL COMMENT '省名',
  `city_name` varchar(50) DEFAULT NULL COMMENT '市名',
  `zone_name` varchar(50) DEFAULT NULL COMMENT '区名',
  `city_fullname` varchar(100) DEFAULT NULL COMMENT '市全名',
  `id_type` varchar(50) DEFAULT NULL COMMENT '证件类型',
  PRIMARY KEY (`idnum_city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6724 DEFAULT CHARSET=utf8 COMMENT='身份证所属城市';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_post_record`
--

DROP TABLE IF EXISTS `message_post_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_post_record` (
  `message_post_record_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '消息发送ID',
  `enroll_operation_record_id` bigint(20) DEFAULT NULL COMMENT '报名操作记录ID',
  `enroll_id` bigint(20) DEFAULT NULL COMMENT '报名ID',
  `school_id` bigint(20) DEFAULT NULL COMMENT '中心ID',
  `event_type` varchar(50) DEFAULT NULL COMMENT '事件类型',
  `message_name` varchar(250) DEFAULT NULL COMMENT '消息名称',
  `message_detail` text COMMENT '消息内容',
  `receive` varchar(50) DEFAULT NULL COMMENT '接收方',
  `receive_cnname` varchar(50) DEFAULT NULL COMMENT '接收方姓名',
  `sender` varchar(50) DEFAULT NULL COMMENT '发送方',
  `post_type` varchar(50) DEFAULT NULL COMMENT '发送类型',
  `post_ok` tinyint(1) DEFAULT NULL COMMENT '是否已发送',
  `post_time` datetime DEFAULT NULL COMMENT '发送时间',
  `post_try_times` int(11) DEFAULT NULL COMMENT '发送次数',
  `post_remark` text COMMENT '发送情况备注',
  PRIMARY KEY (`message_post_record_id`),
  KEY `Relationship_12_FK` (`enroll_operation_record_id`),
  KEY `FK_message_post_record_enroll` (`enroll_id`),
  KEY `FK_message_post_record_school` (`school_id`),
  CONSTRAINT `FK_message_post_record_enroll` FOREIGN KEY (`enroll_id`) REFERENCES `enroll` (`enroll_id`),
  CONSTRAINT `FK_message_post_record_enroll_operation_record` FOREIGN KEY (`enroll_operation_record_id`) REFERENCES `enroll_operation_record` (`enroll_operation_record_id`),
  CONSTRAINT `FK_message_post_record_school` FOREIGN KEY (`school_id`) REFERENCES `school` (`school_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='消息发送日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_template`
--

DROP TABLE IF EXISTS `message_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_template` (
  `message_template_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '消息模板ID',
  `message_name` varchar(250) DEFAULT NULL COMMENT '消息名称',
  `message_detail` text COMMENT '消息内容',
  `message_detail_email` text COMMENT '邮件消息内容',
  `message_detail_weixin` text COMMENT '微信消息内容',
  `message_detail_duanxin` text COMMENT '短信消息内容',
  PRIMARY KEY (`message_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='各种消息的模板，如提交报名表成功、报名录取通知、建议改报通知等等。每种消息可能有用于email、微信、短信。messag';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `opt_log`
--

DROP TABLE IF EXISTS `opt_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opt_log` (
  `opt_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '操作日志ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `opt_time` datetime DEFAULT NULL COMMENT '操作时间',
  `opt_recordid` bigint(20) DEFAULT NULL COMMENT '操作对象记录ID',
  `opt_table` varchar(50) DEFAULT NULL COMMENT '操作对象表',
  `opt_type` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `opt_detail` text COMMENT '操作内容',
  `opt_user_name` varchar(50) DEFAULT NULL COMMENT '操作者姓名',
  `save_time` datetime DEFAULT NULL COMMENT '记录时间',
  PRIMARY KEY (`opt_log_id`),
  KEY `FK_opt_log_sys_user` (`user_id`),
  CONSTRAINT `FK_opt_log_sys_user` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='操作日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `option_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '选项ID',
  `option_name` varchar(50) DEFAULT NULL COMMENT '选项名字',
  `option_items` varchar(1024) DEFAULT NULL COMMENT '可选项',
  `option_group` varchar(50) DEFAULT NULL COMMENT '选项分类',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='各种选项，具体包括：证件类型、身份、职业、学历、法工岗位、语言、报到备注事项等';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `school_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '中心ID',
  `school_name` varchar(100) DEFAULT NULL COMMENT '中心名称',
  `region` varchar(50) DEFAULT NULL COMMENT '区域',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `address` varchar(250) DEFAULT NULL COMMENT '地址',
  `postcode` varchar(10) DEFAULT NULL COMMENT '邮编',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `enroll_phone` varchar(50) DEFAULT NULL COMMENT '报名电话',
  `office_phone` varchar(50) DEFAULT NULL COMMENT '办公电话',
  `website` varchar(100) DEFAULT NULL COMMENT '中心网站',
  `enrollurl` varchar(100) DEFAULT NULL COMMENT '报名链接',
  `school_photo` varchar(250) DEFAULT NULL COMMENT '中心照片',
  `poi_latitude_longitude` varchar(100) DEFAULT NULL COMMENT '地图标识点经纬度',
  `poi_url` varchar(100) DEFAULT NULL COMMENT '地图标识点URL',
  `poi_barcode` varchar(250) DEFAULT NULL COMMENT '地图标识点二维码',
  `poi_image` varchar(250) DEFAULT NULL COMMENT '地图标识点图片',
  `trafficguide` text COMMENT '交通指引',
  `checkin_note` text COMMENT '报到须知',
  `email_config_json` text COMMENT '收发邮件设置',
  `enrollmentConfig_json` text COMMENT '招生人数',
  `status` varchar(20) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='中心';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_abroad`
--

DROP TABLE IF EXISTS `school_abroad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_abroad` (
  `school_abroad_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '国外中心ID',
  `school_name` varchar(100) DEFAULT NULL COMMENT '中心名称',
  `address` varchar(250) DEFAULT NULL COMMENT '地址',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `office_phone` varchar(50) DEFAULT NULL COMMENT '办公电话',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  PRIMARY KEY (`school_abroad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国外中心';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_enroll_limit`
--

DROP TABLE IF EXISTS `school_enroll_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_enroll_limit` (
  `enroll_limit_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '招生限制ID',
  `enroll_limit_type` varchar(20) DEFAULT NULL COMMENT '招生限制类型',
  `enroll_limit_name` varchar(50) DEFAULT NULL COMMENT '招生限制名称',
  `enroll_limit_value` varchar(50) DEFAULT NULL COMMENT '招生限制值',
  `enroll_limit_not` tinyint(1) DEFAULT NULL COMMENT '招生限制值取非',
  `enroll_limit_desp` text COMMENT '招生限制描述',
  PRIMARY KEY (`enroll_limit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='招生限制，包括身份证限制、境外人士限制等';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_enroll_limit_school`
--

DROP TABLE IF EXISTS `school_enroll_limit_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_enroll_limit_school` (
  `school_id` bigint(20) NOT NULL COMMENT '中心ID',
  `enroll_limit_id` bigint(20) NOT NULL COMMENT '招生限制ID',
  PRIMARY KEY (`school_id`,`enroll_limit_id`),
  KEY `FK_school_enroll_limit_school_school_enroll_limit` (`enroll_limit_id`),
  CONSTRAINT `FK_school_enroll_limit_school_school` FOREIGN KEY (`school_id`) REFERENCES `school` (`school_id`),
  CONSTRAINT `FK_school_enroll_limit_school_school_enroll_limit` FOREIGN KEY (`enroll_limit_id`) REFERENCES `school_enroll_limit` (`enroll_limit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='中心招生限制与中心关联';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `study_work_record`
--

DROP TABLE IF EXISTS `study_work_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_work_record` (
  `study_work_record_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '上课服务记录ID',
  `course_id` bigint(20) DEFAULT NULL COMMENT '课程ID',
  `cn_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `en_name` varchar(50) DEFAULT NULL COMMENT '英文名',
  `id_type` varchar(50) DEFAULT NULL COMMENT '证件类型',
  `id_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `schoolId` bigint(20) DEFAULT NULL COMMENT '中心ID',
  `school_name` varchar(100) DEFAULT NULL COMMENT '中心名称',
  `school_country` varchar(50) DEFAULT NULL COMMENT '中心所在国家',
  `course_date_from` date DEFAULT NULL COMMENT '课程开始时间',
  `course_date_to` date DEFAULT NULL COMMENT '课程结束时间',
  `course_type_name` varchar(50) DEFAULT NULL COMMENT '课程类型',
  `course_days` int(11) DEFAULT NULL COMMENT '课程天数',
  `course_class` varchar(50) DEFAULT NULL COMMENT '课程种类',
  `teacher` varchar(250) DEFAULT NULL COMMENT '带课老师',
  `course_finished` tinyint(1) DEFAULT NULL COMMENT '是否完成课程',
  `leavetime` varchar(250) DEFAULT NULL COMMENT '离开时间',
  `leavecause` varchar(250) DEFAULT NULL COMMENT '离开原因',
  `remark` text COMMENT '备注',
  `studentOrWorker` varchar(10) DEFAULT NULL COMMENT '学员还是法工',
  `work` varchar(50) DEFAULT NULL COMMENT '岗位',
  `full_time` tinyint(1) DEFAULT NULL COMMENT '是否全程',
  `date_from` date DEFAULT NULL COMMENT '开始时间',
  `date_to` date DEFAULT NULL COMMENT '结束时间',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updator` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`study_work_record_id`),
  KEY `cn_name` (`cn_name`),
  KEY `id_num` (`id_num`),
  KEY `student_worker` (`studentOrWorker`),
  KEY `course_days` (`course_days`),
  KEY `course_finished` (`course_finished`),
  KEY `full_time` (`full_time`),
  KEY `FK_study_work_record_course` (`course_id`),
  CONSTRAINT `FK_study_work_record_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74016 DEFAULT CHARSET=utf8 COMMENT='上课和服务记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `study_work_record_uncheck`
--

DROP TABLE IF EXISTS `study_work_record_uncheck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_work_record_uncheck` (
  `study_work_record_uncheck_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '待核实上课服务记录ID',
  `cn_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `en_name` varchar(50) DEFAULT NULL COMMENT '英文名',
  `id_type` varchar(50) DEFAULT NULL COMMENT '证件类型',
  `id_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `country` varchar(50) DEFAULT NULL COMMENT '国家',
  `schoolId` bigint(20) DEFAULT NULL COMMENT '中心ID',
  `school_name` varchar(100) DEFAULT NULL COMMENT '中心名称',
  `school_country` varchar(50) DEFAULT NULL COMMENT '中心所在国家',
  `course_date_from` date DEFAULT NULL COMMENT '课程开始时间',
  `course_date_to` date DEFAULT NULL COMMENT '课程结束时间',
  `course_type_name` varchar(50) DEFAULT NULL COMMENT '课程类型',
  `course_days` int(11) DEFAULT NULL COMMENT '课程天数',
  `course_class` varchar(50) DEFAULT NULL COMMENT '课程种类',
  `teacher` varchar(250) DEFAULT NULL COMMENT '带课老师',
  `studentOrWorker` varchar(10) DEFAULT NULL COMMENT '学员还是法工',
  `work` varchar(50) DEFAULT NULL COMMENT '岗位',
  `full_time` tinyint(1) DEFAULT NULL COMMENT '是否全程',
  `date_from` date DEFAULT NULL COMMENT '开始时间',
  `date_to` date DEFAULT NULL COMMENT '结束时间',
  `check_status` varchar(20) DEFAULT NULL COMMENT '审核状态',
  `check_remark` text COMMENT '审核备注',
  `check_user_name` varchar(50) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL COMMENT '审核时间',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_updator` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`study_work_record_uncheck_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='待核实上课服务记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_authority`
--

DROP TABLE IF EXISTS `sys_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_authority` (
  `authority_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `authority_name` varchar(50) DEFAULT NULL COMMENT '权限名称',
  `authority_code` varchar(50) DEFAULT NULL COMMENT '权限编码',
  `authority_order` int(11) DEFAULT NULL COMMENT '权限序号',
  `authority_class` varchar(50) DEFAULT NULL COMMENT '权限分类',
  `authority_class_order` int(11) DEFAULT NULL COMMENT '权限分类序号',
  `limit_resource_type` varchar(50) DEFAULT NULL COMMENT '限制的资源类型',
  PRIMARY KEY (`authority_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_limit_authority`
--

DROP TABLE IF EXISTS `sys_limit_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_limit_authority` (
  `user_authority_limit_id` bigint(20) NOT NULL COMMENT '用户权限范围限制ID',
  `authority_id` bigint(20) NOT NULL COMMENT '权限ID',
  PRIMARY KEY (`user_authority_limit_id`,`authority_id`),
  KEY `FK_sys_limit_authority_sys_authority` (`authority_id`),
  CONSTRAINT `FK_sys_limit_authority_sys_authority` FOREIGN KEY (`authority_id`) REFERENCES `sys_authority` (`authority_id`),
  CONSTRAINT `FK_sys_limit_authority_sys_user_authority_limit` FOREIGN KEY (`user_authority_limit_id`) REFERENCES `sys_user_authority_limit` (`user_authority_limit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='限制权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_limit_course_type`
--

DROP TABLE IF EXISTS `sys_limit_course_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_limit_course_type` (
  `course_type_id` bigint(20) NOT NULL COMMENT '课程类型ID',
  `user_authority_limit_id` bigint(20) NOT NULL COMMENT '用户权限范围限制ID',
  PRIMARY KEY (`course_type_id`,`user_authority_limit_id`),
  KEY `FK_sys_limit_course_type_sys_user_authority_limit` (`user_authority_limit_id`),
  CONSTRAINT `FK_sys_limit_course_type_course_type` FOREIGN KEY (`course_type_id`) REFERENCES `course_type` (`course_type_id`),
  CONSTRAINT `FK_sys_limit_course_type_sys_user_authority_limit` FOREIGN KEY (`user_authority_limit_id`) REFERENCES `sys_user_authority_limit` (`user_authority_limit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='限制课程类型';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_limit_school`
--

DROP TABLE IF EXISTS `sys_limit_school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_limit_school` (
  `user_authority_limit_id` bigint(20) NOT NULL COMMENT '用户权限范围限制ID',
  `school_id` bigint(20) NOT NULL COMMENT '中心ID',
  PRIMARY KEY (`user_authority_limit_id`,`school_id`),
  KEY `FK_sys_limit_school_school` (`school_id`),
  CONSTRAINT `FK_sys_limit_school_school` FOREIGN KEY (`school_id`) REFERENCES `school` (`school_id`),
  CONSTRAINT `FK_sys_limit_school_sys_user_authority_limit` FOREIGN KEY (`user_authority_limit_id`) REFERENCES `sys_user_authority_limit` (`user_authority_limit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='限制中心';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_limit_tag`
--

DROP TABLE IF EXISTS `sys_limit_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_limit_tag` (
  `tag_id` bigint(20) NOT NULL COMMENT '标签ID',
  `user_authority_limit_id` bigint(20) NOT NULL COMMENT '用户权限范围限制ID',
  PRIMARY KEY (`tag_id`,`user_authority_limit_id`),
  KEY `FK_sys_limit_tag_sys_user_authority_limit` (`user_authority_limit_id`),
  CONSTRAINT `FK_sys_limit_tag_sys_user_authority_limit` FOREIGN KEY (`user_authority_limit_id`) REFERENCES `sys_user_authority_limit` (`user_authority_limit_id`),
  CONSTRAINT `FK_sys_limit_tag_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='限制文章标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_role_authority`
--

DROP TABLE IF EXISTS `sys_role_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_role_authority` (
  `authority_id` bigint(20) NOT NULL COMMENT '权限ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`authority_id`,`role_id`),
  KEY `FK_sys_role_authority_sys_role` (`role_id`),
  CONSTRAINT `FK_sys_role_authority_sys_authority` FOREIGN KEY (`authority_id`) REFERENCES `sys_authority` (`authority_id`),
  CONSTRAINT `FK_sys_role_authority_sys_role` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `cn_name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `id_type` varchar(50) DEFAULT NULL COMMENT '证件类型',
  `id_num` varchar(50) DEFAULT NULL COMMENT '证件号码',
  `city` varchar(50) DEFAULT NULL COMMENT '城市',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `email` varchar(50) DEFAULT NULL COMMENT 'email',
  `weixinunionid` varchar(50) DEFAULT NULL COMMENT '微信',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_user_authority_limit`
--

DROP TABLE IF EXISTS `sys_user_authority_limit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_authority_limit` (
  `user_authority_limit_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户权限范围限制ID',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `limit_resource_type` varchar(50) DEFAULT NULL COMMENT '限制的资源类型',
  PRIMARY KEY (`user_authority_limit_id`),
  KEY `FK_sys_user_authority_limit_sys_user` (`user_id`),
  CONSTRAINT `FK_sys_user_authority_limit_sys_user` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定义用户的哪些权限只能对什么样的数据操作。如限制用户只能管理某些中心、只能创建某些类型的课程等';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user_role` (
  `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户角色ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `date_from` date DEFAULT NULL COMMENT '开始时间',
  `date_to` date DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`user_role_id`),
  KEY `FK_sys_user_role_sys_role` (`role_id`),
  KEY `FK_sys_user_role_sys_user` (`user_id`),
  CONSTRAINT `FK_sys_user_role_sys_role` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`role_id`),
  CONSTRAINT `FK_sys_user_role_sys_user` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `tag_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '标签ID',
  `tag_name` varchar(100) DEFAULT NULL COMMENT '标签名称',
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='标签';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'new_neiguan'
--
/*!50003 DROP FUNCTION IF EXISTS `ng_enroll_iswaiting` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `ng_enroll_iswaiting`(
	in_course_id bigint,
    in_sudent_or_worker varchar(10),
    in_gender varchar(10),
    in_is_old tinyint(1)
) RETURNS tinyint(1)
BEGIN
    DECLARE c int;
    DECLARE b boolean;
    
    if (in_sudent_or_worker='WORKER') then
		return false;
    end if;
    
	if (in_gender='MALE' AND in_is_old=0) then
		SELECT male_new into c FROM course WHERE course_id = in_course_id;
	elseif (in_gender='MALE' AND in_is_old=1) THEN
		SELECT male_old into c FROM course WHERE course_id = in_course_id;
	elseif (in_gender='FEMALE' AND in_is_old=0) THEN
		SELECT female_new into c FROM course WHERE course_id = in_course_id;
	elseif (in_gender='FEMALE' AND in_is_old=1) THEN
		SELECT female_old into c FROM course WHERE course_id = in_course_id;
	end if;
    
    if (c is null) then
		set c=0;
	end if;
    
	SELECT count(*)>=c into b FROM enroll 
	WHERE course_id = in_course_id 
	AND studentOrWorker = 'STUDENT' 
	AND enroll_status = 'VALID' 
	AND gender = in_gender
	AND isOld = in_is_old;

	RETURN b;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ng_getCourseEnrollCount` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `ng_getCourseEnrollCount`(
	in_course_id bigint,
    in_sudent_or_worker varchar(10),
    in_gender varchar(10),
    in_is_old tinyint(1)
) RETURNS int(11)
BEGIN
    DECLARE c INT;
    
    if ('WORKER'=in_sudent_or_worker) THEN
		SELECT count(*) into c FROM enroll 
		WHERE course_id = in_course_id AND enroll_status = 'VALID' 
		AND studentOrWorker = in_sudent_or_worker 
		AND gender = in_gender;
	else
		SELECT count(*) into c FROM enroll 
		WHERE course_id = in_course_id AND enroll_status = 'VALID' 
		AND studentOrWorker = in_sudent_or_worker 
		AND gender = in_gender
		AND isOld = in_is_old;
    end if;

	RETURN c;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ng_getCourseEnrollFlag` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `ng_getCourseEnrollFlag`(
	in_course_id bigint,
    in_sudent_or_worker varchar(10),
    in_gender varchar(10),
    in_is_old tinyint(1)
) RETURNS varchar(20) CHARSET utf8
BEGIN
	DECLARE fullCount int;
	DECLARE spareCount int;
	DECLARE enrollCount int;
    
	DECLARE waitLongCount int;
	DECLARE fillingupCount int;
    
	if (in_sudent_or_worker='STUDENT' AND in_gender='MALE' AND in_is_old=0) then
		SELECT male_new, male_new_spare into fullCount, spareCount FROM course WHERE course_id = in_course_id AND `status`='STANDBY' AND (is_enroll_stoped is null OR is_enroll_stoped=0) AND CURDATE() between enroll_date_from AND enroll_date_to;
	elseif (in_sudent_or_worker='STUDENT' AND in_gender='MALE' AND in_is_old=1) THEN
		SELECT male_old, male_old_spare into fullCount, spareCount FROM course WHERE course_id = in_course_id AND `status`='STANDBY' AND (is_enroll_stoped is null OR is_enroll_stoped=0) AND CURDATE() between enroll_date_from AND enroll_date_to;
	elseif (in_sudent_or_worker='STUDENT' AND in_gender='FEMALE' AND in_is_old=0) THEN
		SELECT female_new, female_new_spare into fullCount, spareCount FROM course WHERE course_id = in_course_id AND `status`='STANDBY' AND (is_enroll_stoped is null OR is_enroll_stoped=0) AND CURDATE() between enroll_date_from AND enroll_date_to;
	elseif (in_sudent_or_worker='STUDENT' AND in_gender='FEMALE' AND in_is_old=1) THEN
		SELECT female_old, female_old_spare into fullCount, spareCount FROM course WHERE course_id = in_course_id AND `status`='STANDBY' AND (is_enroll_stoped is null OR is_enroll_stoped=0) AND CURDATE() between enroll_date_from AND enroll_date_to;
	elseif (in_sudent_or_worker='WORKER' AND in_gender='MALE') THEN
		SELECT male_worker,0 into fullCount, spareCount FROM course WHERE course_id = in_course_id AND `status`='STANDBY' AND (is_enroll_stoped is null OR is_enroll_stoped=0) AND CURDATE() between enroll_date_from AND enroll_date_to;
	elseif (in_sudent_or_worker='WORKER' AND in_gender='FEMALE') THEN
		SELECT female_worker,0 into fullCount, spareCount FROM course WHERE course_id = in_course_id AND `status`='STANDBY' AND (is_enroll_stoped is null OR is_enroll_stoped=0) AND CURDATE() between enroll_date_from AND enroll_date_to;
	end if;
	
	if (fullCount is null OR fullCount<=0) THEN 
		RETURN 'CLOSE';
	end if;
    
	if (spareCount=null) THEN 
		SET spareCount=0;
	end if;

	select ng_getCourseEnrollCount(in_course_id,in_sudent_or_worker,in_gender,in_is_old) into enrollCount;

	if (enrollCount=null) THEN 
		SET enrollCount=0;
	end if;

	
    set waitLongCount=fullCount+spareCount/2;
    set fillingupCount=fullCount*2/3;
    
	if (enrollCount<fillingupCount) THEN
		RETURN 'OPEN';
	elseif (enrollCount>=fillingupCount AND enrollCount<fullCount) THEN 
		RETURN 'FILLINGUP';
	elseif (enrollCount>=fullCount AND enrollCount<waitLongCount) THEN 
		RETURN 'WAITSHORT';
	elseif (enrollCount>=waitLongCount AND enrollCount<(fullCount+spareCount)) THEN
		RETURN 'WAITLONG';
	else
		RETURN 'CLOSE';
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `ng_getIdnumCityFullname` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` FUNCTION `ng_getIdnumCityFullname`(
    in_idtype varchar(50),
    in_idnum varchar(50)
) RETURNS varchar(100) CHARSET utf8
BEGIN

DECLARE cityFullname varchar(100);

select `city_fullname` into cityFullname FROM `idnum_city` where `zone_id`=left(in_idnum,6) and id_type=in_idtype limit 1;

RETURN cityFullname;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_course_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ng_course_list`(
	v_schoolId bigint,
	v_dateFrom Date
)
BEGIN
select
	c.course_id, ct.course_type_name, s.school_name, c.status, 
    c.course_date_from,  c.course_date_to,  c.enroll_date_from, c.enroll_date_to, 
     IF(CURDATE() < c.course_date_from, 'BEFORE', IF(CURDATE() < c.course_date_to, 'IN', 'AFTER')) AS course_time_slot, 
     IF((c.male_new+c.male_old)=0, 'FEMALE', IF((c.female_new+c.female_old)=0, 'MALE', 'BOTH')) AS student_type, 
     IF(c.is_enroll_stoped, 'CLOSE', IF(now()<c.enroll_date_from, 'WILL', IF(now()>c.enroll_date_to, 'CLOSE', 'OPEN'))) AS course_enroll_status, 
     c.male_new,
     c.male_new+c.male_new_spare as male_new_total,
     (SELECT count(*) FROM enroll WHERE course_id = c.course_id AND enroll_status = 'VALID' AND studentOrWorker = 'STUDENT' AND gender = 'MALE'	AND isOld = 0) as male_new_enroll_count,
     c.male_old,
     c.male_old+c.male_old_spare as male_old_total,
     (SELECT count(*) FROM enroll WHERE course_id = c.course_id AND enroll_status = 'VALID' AND studentOrWorker = 'STUDENT' AND gender = 'MALE'	AND isOld = 1) as male_old_enroll_count,
     c.female_new,
     c.female_new+c.female_new_spare as female_new_total,
     (SELECT count(*) FROM enroll WHERE course_id = c.course_id AND enroll_status = 'VALID' AND studentOrWorker = 'STUDENT' AND gender = 'FEMALE'	AND isOld = 0) as female_new_enroll_count,
     c.female_old,
     c.female_old+c.female_old_spare as female_old_total,
     (SELECT count(*) FROM enroll WHERE course_id = c.course_id AND enroll_status = 'VALID' AND studentOrWorker = 'STUDENT' AND gender = 'FEMALE'	AND isOld = 1) as female_old_enroll_count,
     c.male_worker,
     (SELECT count(*) FROM enroll WHERE course_id = c.course_id AND enroll_status = 'VALID' AND studentOrWorker = 'WORKER' AND gender = 'MALE' ) as male_worker_enroll_count,
     c.female_worker,
     (SELECT count(*) FROM enroll WHERE course_id = c.course_id AND enroll_status = 'VALID' AND studentOrWorker = 'WORKER' AND gender = 'FEMALE' ) as female_worker_enroll_count,
     ng_getCourseEnrollFlag(c.course_id, 'STUDENT', 'MALE', 0) AS enrollFlag_male_new, 
     ng_getCourseEnrollFlag(c.course_id, 'STUDENT', 'MALE', 1) AS enrollFlag_male_old, 
     ng_getCourseEnrollFlag(c.course_id, 'STUDENT', 'FEMALE', 0) AS enrollFlag_female_new, 
     ng_getCourseEnrollFlag(c.course_id, 'STUDENT', 'FEMALE', 1) AS enrollFlag_female_old, 
     ng_getCourseEnrollFlag(c.course_id, 'WORKER', 'MALE', 1) AS enrollFlag_maleworker, 
     ng_getCourseEnrollFlag(c.course_id, 'WORKER', 'FEMALE', 1) AS enrollFlag_femaleworker 
from course c
left join school s on c.school_id=s.school_id
left join course_type ct on c.course_type_id=ct.course_type_id
where c.school_id=v_schoolId AND c.course_date_from>=v_dateFrom
order by c.course_date_from desc
;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_get_person_info` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ng_get_person_info`(
    in_cnname varchar(50),
    in_idnum varchar(50)
)
BEGIN

select a1.*,b1.*,c1.*,d1.*,e1.*,f1.*,g1.*
from 
(
select a.cn_name, a.en_name, a.id_num, a.id_type,a.gender,a.mobile,a.email,a.birthday,a.country from study_work_record a 
where a.cn_name=in_cnname and a.id_num=in_idnum
order by course_date_from desc limit 1
) as a1
,
(
select a.course_date_from as first_study_date_from, a.school_name as first_study_school, a.teacher as first_study_teacher from study_work_record a 
inner join (
select min(course_date_from) course_date_from from study_work_record
where cn_name=in_cnname and id_num=in_idnum and studentOrWorker='STUDENT' and course_finished=1 and full_time=1 AND course_days>8
) b 
on a.course_date_from = b.course_date_from
where a.cn_name=in_cnname and a.id_num=in_idnum and a.studentOrWorker='STUDENT'
) as b1
,
(
select a.course_date_from as last_study_date_from, a.school_name as last_study_school, a.teacher as last_study_teacher from study_work_record a 
inner join (
select max(course_date_from) course_date_from from study_work_record
where cn_name=in_cnname and id_num=in_idnum and studentOrWorker='STUDENT' and course_finished=1 and full_time=1 AND course_days>8
) b 
on a.course_date_from = b.course_date_from
where a.cn_name=in_cnname and a.id_num=in_idnum and a.studentOrWorker='STUDENT'
) as c1
,
(
select sum(studentOrWorker='STUDENT') as study_count
, sum(studentOrWorker='STUDENT' AND course_days=10) as study_count_10day
, sum(studentOrWorker='STUDENT' AND course_days=9) as study_count_SatipatthanaSutta
, sum(studentOrWorker='STUDENT' AND course_days=20) as study_count_20day
, sum(studentOrWorker='STUDENT' AND course_days=30) as study_count_30day
, sum(studentOrWorker='STUDENT' AND course_days=45) as study_count_45day
, sum(studentOrWorker='WORKER') as service_count 
, sum(studentOrWorker='WORKER' AND course_days=10) as service_count_10day
, sum(studentOrWorker='WORKER' AND course_days=9) as service_count_SatipatthanaSutta
, sum(studentOrWorker='WORKER' AND course_days=20) as service_count_20day
, sum(studentOrWorker='WORKER' AND course_days=30) as service_count_30day
, sum(studentOrWorker='WORKER' AND course_days=45) as service_count_45day
from study_work_record
where cn_name=in_cnname and id_num=in_idnum and course_finished=1 and full_time=1 AND course_days>8
) as d1,
(
SELECT count(*) as bad_enroll_count, ifnull(sum(release_date>now()),0) as unrelease_bad_enroll_count
FROM `new_neiguan`.`bad_enroll`
where cn_name=in_cnname and id_num=in_idnum
) as e1,
(
SELECT count(*) as code_count, ifnull(sum(release_date>now()),0) as unrelease_code_count
FROM `new_neiguan`.`code`
where cn_name=in_cnname and id_num=in_idnum
) as f1,
(
SELECT sum(studentOrWorker='STUDENT' AND enroll_status='VALID') as study_enroll_count
, sum(studentOrWorker='STUDENT' AND enroll_status='VALID' AND c.course_date_from > now()) as to_study_enroll_count
, sum(studentOrWorker='STUDENT' AND enroll_status='VALID' AND c.course_date_from < now() AND c.course_date_to > now()) as studing_enroll_count
, sum(studentOrWorker='STUDENT' AND enroll_status='CANCEL') as cancel_study_enroll_count
, sum(studentOrWorker='WORKER' AND enroll_status='VALID') as work_enroll_count
, sum(studentOrWorker='WORKER' AND enroll_status='VALID' AND c.course_date_from > now()) as to_work_enroll_count
, sum(studentOrWorker='WORKER' AND enroll_status='VALID' AND c.course_date_from < now() AND c.course_date_to > now()) as working_enroll_count
, sum(studentOrWorker='WORKER' AND enroll_status='CANCEL') as cancel_work_enroll_count
FROM enroll e
inner join course c on e.course_id=c.course_id
where cn_name=in_cnname and id_num=in_idnum
) as g1
;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_save_user_role` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ng_save_user_role`(
	v_userId bigint,
	v_roleId bigint,
	v_dateFrom Date,
	v_dateTo Date
)
BEGIN

if exists (select * from sys_user_role where user_id=v_userId and role_id=v_roleId) then
	UPDATE `sys_user_role` SET `date_from` = v_dateFrom,`date_to` = v_dateTo
	where user_id=v_userId and role_id=v_roleId;
else
	INSERT INTO `sys_user_role` (`role_id`,`user_id`,`date_from`,`date_to`)
	VALUES (v_roleId,v_userId,v_dateFrom,v_dateTo);
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_test_clear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ng_test_clear`()
BEGIN

delete from message_post_record;
delete from enroll_operation_record;

delete from bad_enroll;
delete from school_enroll_limit_school;

delete from school_enroll_limit;

delete from `code`;
delete from opt_log;

delete from school_abroad;
delete from `study_work_record`;
delete from `study_work_record_uncheck`;

delete from enroll;
delete FROM course;
delete FROM school;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_test_init` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ng_test_init`()
BEGIN
	DECLARE v_male_name varchar(50);
	DECLARE v_male_idnum varchar(50);
	DECLARE v_male_email varchar(50);
	declare i int;

	SET v_male_name = '张三';
	SET v_male_idnum = '420110198811206167';
	SET v_male_email = 'aa@163.com';

	CALL `ng_test_clear`();
	
	INSERT INTO `school_enroll_limit` (`enroll_limit_id`, `enroll_limit_type`, `enroll_limit_name`, `enroll_limit_value`, `enroll_limit_not`, `enroll_limit_desp`) VALUES 
	(1,'IDNUM','新疆','65',0,'不接收新疆籍学员'),
	(2,'IDTYPE','台湾身份证','台湾身份证',0,'不接收台湾籍学员'),
	(3,'NATIONALITY','非中国国籍','中国',1,'不接收非中国国籍学员');

	
	INSERT INTO `school` (`school_id`, `school_name`, `region`, `city`, `address`, `postcode`, `email`, `enroll_phone`, `office_phone`, `website`, `enrollurl`, `school_photo`, `poi_latitude_longitude`, `poi_url`, `poi_barcode`, `poi_image`, `trafficguide`, `checkin_note`, `email_config_json`, `enrollmentConfig_json`, `status`) VALUES 
	(1,'南禅寺内观中心','cn','福建省龙岩市','福建省龙岩市长汀县南禅寺','366300','2710490057@qq.com','13348341989','13348341989','','','/file/school/1/SchoolPhoto_C385C6D7054602E9B59D70B4636D103D.jpg','','http://j.map.baidu.com/XSo2H','/file/school/1/PoiBarcode_950A41D8EB56931D9E40C62228AE76A1.png','/file/school/1/PoiImage_C1A734C79F034EF85D6BF6AC960142E4.png','<p> 长汀隶属福建省龙岩市，在福建西部靠近江西瑞金。</p><p>长汀有火车站，也可以坐火车到龙岩、赣州、瑞金或南昌等地后再转车到长汀南站。<br></p><p>坐飞机的话可到江西赣州（黄金机场）、龙岩（连城的冠豸山机场）或厦门，再转车过来。</p><p>厦门，龙岩、赣州到长汀南火车站1到2个小时，汽车4个多小时，连城到长汀2个多小时，厦门（湖滨南长途汽车站0592-968828）到长汀4小时。</p><p>到长汀南站打的或坐摩托到南禅寺，15分钟到达，打的30-40元，摩托15-20元；公交车所需车程50分钟。3路公交车在县政府下到对面上8路公交车在南禅寺下车即可。\r\n</p>','		<p>\r\n			</p><h3>一、请务必携带</h3>\r\n			<p>\r\n				1、 一张1寸或2寸登记照、身份证（或护照）、达到65岁的学员需携带健康证明。<br>\r\n				2、水杯及洗漱用品、洗澡用具(中心提供沐浴) 。<br>\r\n				3、请尽量穿不打滑的鞋。<br>\r\n				4、冬季寒冷，请备好厚棉袜、棉拖鞋、手套、帽子、围巾、羽绒服及保暖、舒适的宽松衣服。请勿携带会发出磨擦声音的衣物（如尼龙或胶质衣物）。\r\n			</p>\r\n		      <blockquote>\r\n		        2～4月雨季时，请考虑携带足够整个课程所需更换的衣物。\r\n		      </blockquote>\r\n		<p></p>		\r\n\r\n		<p>\r\n			</p><h3>二、以下物品中心已有提供</h3>\r\n			<p>\r\n				1、餐具。<br>\r\n				2、床单、被套、蚊帐等床上用品。<br>\r\n				3、坐垫、盖腿用的毛巾被或毛毯。<br>\r\n				4、拖鞋、洗脸盆等。\r\n			</p>\r\n		<p></p>		\r\n\r\n		<p>\r\n			</p><h3>三、学员可自备</h3>\r\n			<p>雨具、睡袋、披肩等。</p>\r\n		<p></p>		\r\n\r\n		<p>\r\n			</p><h3>四、如个人有需要可考虑携带</h3>\r\n			<p>\r\n				1、经医生诊断之处方药。<br>\r\n				2、眼罩、耳塞（请勿在禅堂使用）。\r\n			</p>\r\n		<p></p>		\r\n		\r\n		<p>\r\n			</p><h3>五、为了你自己能在禅修课程中达成最大的收益，请务必注意</h3>\r\n			<p>	1、请勿携带：（否则在课程期间中心会代为保管）</p>\r\n			<ul><li> 书籍、书写文具、收音机、录音设备、MP3、笔记本电脑等；</li>\r\n			  <li> 食物，尤其是荤食或含有酒精、蛋的食品；</li>\r\n			  <li> 香烟、酒类、安眠药、毒品、镇静剂或其它具镇定作用或非法之药品；</li>\r\n			  <li> 护身符、水晶、画像、念珠、法器等宗教物品；</li>\r\n			  <li> 贵重物品或金银珠宝；</li>\r\n			  <li> 动物毛皮制成之衣物、用品。</li>\r\n			</ul>\r\n	      <p>2、禅修期间必须严格遵守禅修规定、遵守老师的指导、禁语、不与外界联系，尤其是必须全程参与直至课程结束，不得中途退出。课程在第11天早晨约9:30左右结束。</p>\r\n	      <p>3、在课程进行期间，学员不允许与外界接触及使用电话，如有紧急事件，学员家属请致电内观中心代为转告。</p>		\r\n		<p></p>		\r\n','{\"smtpServer\":\"smtp.qq.com\",\"smtpPort\":465,\"needAuth\":true,\"email\":\"2710490057@qq.com\",\"password\":\"nbefmvzlpohzdfcc\"}','{\"single\":{\"oldStudent\":40,\"oldStudentSpare\":20,\"newStudent\":60,\"newStudentSpare\":30,\"worker\":20},\"male\":{\"oldStudent\":20,\"oldStudentSpare\":10,\"newStudent\":30,\"newStudentSpare\":15,\"worker\":10},\"female\":{\"oldStudent\":25,\"oldStudentSpare\":15,\"newStudent\":35,\"newStudentSpare\":20,\"worker\":10}}','OPEN')
	,(2,'厦门内观','cn','','福建厦门海沧区天竺山国家森林公园内真寂寺','361000',NULL,'18150393051','18150350426',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'OPEN')
	,(3,'丹东双灵内观中心','cn','辽宁省丹东市','辽宁省丹东市炮守营四组双灵寺旁','118000',NULL,'17704151971','18341580449',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'交通路线：\r\n \r\n        \r\n\r\n\r\n\r\n1、自驾车路线：丹东双灵寺位于丹东元宝区金山镇炮首营村4组，位于丹东市区西北郊，从沈丹高速路口下来后左拐上304国道，行驶18公里左右，右侧有“双灵寺”路牌，按照指示走即到；或从丹沈高速五龙背收费口下道右拐，5分钟左右，道路左侧即见“双灵寺”路牌。\r\n\r\n2、丹东公交线路：丹东站向左走至丹东市政府与邮政局中间，可见公路车站。乘坐通往孙家的211路公交车至双灵寺下车即可。（注：211走两条路线，要走304国道通往孙家路线的211公交车才可以）投币车2元。\r\n\r\n3、出租车路线：从火车站乘坐出租车40分钟到双灵寺费用在人民币40元左右，或火车在五龙背站下车出租车10多分钟可到双灵寺。\r\n\r\n\r\n \r\n        \r\n4、丹东浪头机场离丹东市区约16公里，机场大巴首末车时间：06:30-21:30，全程：10.0元，到市区后可转公交或出租车到双灵寺，从浪头机场直接坐出租车到双灵寺大约100元左右，车程要一个多小时。\r\n \r\n        \r\n\r\n5、若从沈阳出发，到达丹东大概需要4、5个小时。可乘坐火车到五龙背站下车，距离双灵寺大概十多分钟；也可在沈阳北站附近的客运站，乘坐到丹东的虎跃快客大巴，每隔半小时一趟。沈阳桃仙机场可在机场出口坐直达丹东豪华大巴（每隔1.5-2小时一班车），行程约3小时，票价88。\r\n\r\n6、从大连到丹东大巴需要4个小时，可在站南汽车站（火车站站南广场）坐车，大概1小时一趟，票价100元。\r\n \r\n',NULL,NULL,NULL,'OPEN')
	,(4,'包头内观中心','cn','包头市','包头市滨河新区礼贤路18号黄家医圈（滨河大厦东北方向','014000',NULL,'15848270132','18604728615',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'路线图：\r\n\r\n1、机场大巴20元直接送到目的地。\r\n\r\n2、火车东站16路公交到滨河新区，乘出租车约十元可到。\r\n\r\n3、包头火车站（照潭车站）乘15路公交，转乘16路公交，滨河新区下车，乘出租车约十元可到。',NULL,NULL,NULL,'OPEN')
	,(5,'四川都江堰般若内观中心','cn','','四川成都都江堰蒲阳镇鹿茶般若寺','611800',NULL,'18980613677','18981806170',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,'OPEN')
	,(6,'甘肃庆阳内观中心','cn','甘肃省庆阳市','甘肃省庆阳市西峰区小崆峒山门西侧50米','',NULL,'18093451856','18034612599',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'\r\n甘肃庆阳机场：乘坐机场大巴到市区后，换乘1路公交到达董陈路口，再乘5元出租到达小崆峒风景区\r\n\r\n陕西西安咸阳国际机场：乘坐机场大巴直达庆阳市南客运站后，换乘陈户班车到小崆峒，或乘出租25元直达小崆峒风景区\r\n\r\n西安火车站：火车站对面陕西省长途汽车站乘直达西峰大巴至庆阳南客运站，换乘陈户班车到小崆峒，或乘出租25元直达小崆峒风景区\r\n\r\n庆阳西客运站：乘坐5路公交直达小崆峒\r\n\r\n庆阳北客运站：乘7路或14路公交到东湖公园，再换乘5路公交直达小崆峒\r\n\r\n',NULL,NULL,NULL,'OPEN');

	
	INSERT INTO `school_enroll_limit_school` (`school_id`, `enroll_limit_id`) VALUES 
	(1,1),
	(1,2),
	(1,3);
	
	CALL `ng_test_initCourses`();
    
    INSERT INTO `enroll` (enroll_id, `course_id`,`age`,`student_type`,`recommendation`,`idcity`,`studentOrWorker`,`gender`,`cn_name`,`en_name`,`id_type`,`id_num`,`mobile`,`email`,`mobile2`,`email2`,`photo`,`address`,`city`,`postcode`,`country`,`birthday`,`religion`,`religion_date`,`profession`,`occupation`,`education`,`parent`,`parent_phone`,`canEnglish`,`canChinese`,`canTaiyu`,`canYueyu`,`otherlanguages`,`has_together`,`together`,`together_relationship`,`howKnown`,`linealKin`,`linealKin_phone`,`linealKin_relationship`,`isOld`,`hasExercise`,`hours_per_week`,`canEarlyHelp`,`canWork`,`didPurser`,`had_study_other`,`study_other`,`had_teach`,`teach`,`had_physiclal`,`physiclal`,`had_mental`,`mental`,`had_medicine`,`medicine`,`had_drug`,`drug`,`first_study_date_from`,`first_study_school`,`first_study_teacher`,`last_study_date_from`,`last_study_school`,`last_study_teacher`,`study_count_10day`,`study_count_SatipatthanaSutta`,`study_count_20day`,`study_count_30day`,`study_count_45day`,`service_count_10day`,`service_count_SatipatthanaSutta`,`service_count_20day`,`service_count_30day`,`service_count_45day`,`heath_had_bloodPressure`,`heath_bloodPressure`,`heath_had_diabetes`,`heath_diabetes`,`heath_had_heart`,`heath_heart`,`heath_had_mental`,`heath_mental`,`heath_had_drug`,`heath_drug`,`heath_had_hallucination`,`heath_hallucination`,`heath_had_otherDisease`,`heath_otherDisease`,`heath_had_pregnancy`,`heath_pregnancy`,`heath_had_specialArrangements`,`heath_specialArrangements`,`worker_work`,`worker_schools`,`enroll_status`,`check_status`,`creator`,`create_time`,`last_updator`,`last_update_time`,`enroll_time`) VALUES 
	(1,1,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE',v_male_name,'','二代身份证',v_male_idnum,'15895845287',v_male_email,'',NULL,NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',0,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now());

    
    INSERT INTO `enroll` (enroll_id, `course_id`,`age`,`student_type`,`recommendation`,`idcity`,`studentOrWorker`,`gender`,`cn_name`,`en_name`,`id_type`,`id_num`,`mobile`,`email`,`mobile2`,`email2`,`photo`,`address`,`city`,`postcode`,`country`,`birthday`,`religion`,`religion_date`,`profession`,`occupation`,`education`,`parent`,`parent_phone`,`canEnglish`,`canChinese`,`canTaiyu`,`canYueyu`,`otherlanguages`,`has_together`,`together`,`together_relationship`,`howKnown`,`linealKin`,`linealKin_phone`,`linealKin_relationship`,`isOld`,`hasExercise`,`hours_per_week`,`canEarlyHelp`,`canWork`,`didPurser`,`had_study_other`,`study_other`,`had_teach`,`teach`,`had_physiclal`,`physiclal`,`had_mental`,`mental`,`had_medicine`,`medicine`,`had_drug`,`drug`,`first_study_date_from`,`first_study_school`,`first_study_teacher`,`last_study_date_from`,`last_study_school`,`last_study_teacher`,`study_count_10day`,`study_count_SatipatthanaSutta`,`study_count_20day`,`study_count_30day`,`study_count_45day`,`service_count_10day`,`service_count_SatipatthanaSutta`,`service_count_20day`,`service_count_30day`,`service_count_45day`,`heath_had_bloodPressure`,`heath_bloodPressure`,`heath_had_diabetes`,`heath_diabetes`,`heath_had_heart`,`heath_heart`,`heath_had_mental`,`heath_mental`,`heath_had_drug`,`heath_drug`,`heath_had_hallucination`,`heath_hallucination`,`heath_had_otherDisease`,`heath_otherDisease`,`heath_had_pregnancy`,`heath_pregnancy`,`heath_had_specialArrangements`,`heath_specialArrangements`,`worker_work`,`worker_schools`,`enroll_status`,`check_status`,`creator`,`create_time`,`last_updator`,`last_update_time`,`enroll_time`) VALUES 
	(2,5,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE','测试Email1','','二代身份证','420110198811206167','15895845287','email@163.com','',NULL,NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',0,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now())
	,(3,5,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE','测试Email2','','二代身份证','420110198811206168','15895845288','email@163.com','',NULL,NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',0,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now())
	,(4,5,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE','测试Email3','','二代身份证','420110198811206169','15895845289','email@163.com','',NULL,NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',0,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now())
	,(5,5,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE','测试Email4','','二代身份证','420110198811206160','15895845280','email@163.com','',NULL,NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',0,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now())
    ;
	
    INSERT INTO `enroll` (enroll_id, `course_id`,`age`,`student_type`,`recommendation`,`idcity`,`studentOrWorker`,`gender`,`cn_name`,`en_name`,`id_type`,`id_num`,`mobile`,`email`,`mobile2`,`email2`,`photo`,`address`,`city`,`postcode`,`country`,`birthday`,`religion`,`religion_date`,`profession`,`occupation`,`education`,`parent`,`parent_phone`,`canEnglish`,`canChinese`,`canTaiyu`,`canYueyu`,`otherlanguages`,`has_together`,`together`,`together_relationship`,`howKnown`,`linealKin`,`linealKin_phone`,`linealKin_relationship`,`isOld`,`hasExercise`,`hours_per_week`,`canEarlyHelp`,`canWork`,`didPurser`,`had_study_other`,`study_other`,`had_teach`,`teach`,`had_physiclal`,`physiclal`,`had_mental`,`mental`,`had_medicine`,`medicine`,`had_drug`,`drug`,`first_study_date_from`,`first_study_school`,`first_study_teacher`,`last_study_date_from`,`last_study_school`,`last_study_teacher`,`study_count_10day`,`study_count_SatipatthanaSutta`,`study_count_20day`,`study_count_30day`,`study_count_45day`,`service_count_10day`,`service_count_SatipatthanaSutta`,`service_count_20day`,`service_count_30day`,`service_count_45day`,`heath_had_bloodPressure`,`heath_bloodPressure`,`heath_had_diabetes`,`heath_diabetes`,`heath_had_heart`,`heath_heart`,`heath_had_mental`,`heath_mental`,`heath_had_drug`,`heath_drug`,`heath_had_hallucination`,`heath_hallucination`,`heath_had_otherDisease`,`heath_otherDisease`,`heath_had_pregnancy`,`heath_pregnancy`,`heath_had_specialArrangements`,`heath_specialArrangements`,`worker_work`,`worker_schools`,`enroll_status`,`check_status`,`creator`,`create_time`,`last_updator`,`last_update_time`,`enroll_time`) VALUES 
	(6,4,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE','李六','','二代身份证','1100110198811206167','15895845287','test@163.com','',NULL,NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',0,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now())
	,(7,6,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE','王五','','二代身份证','111116789012345678','15895845288','test1@163.com','',NULL,NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',0,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now())
    ;
	
    
	set i=0; 
	while i < 15 do 
		INSERT INTO `enroll` (enroll_id, `course_id`,`age`,`student_type`,`recommendation`,`idcity`,`studentOrWorker`,`gender`,`cn_name`,`en_name`,`id_type`,`id_num`,`mobile`,`email`,`mobile2`,`email2`,`photo`,`address`,`city`,`postcode`,`country`,`birthday`,`religion`,`religion_date`,`profession`,`occupation`,`education`,`parent`,`parent_phone`,`canEnglish`,`canChinese`,`canTaiyu`,`canYueyu`,`otherlanguages`,`has_together`,`together`,`together_relationship`,`howKnown`,`linealKin`,`linealKin_phone`,`linealKin_relationship`,`isOld`,`hasExercise`,`hours_per_week`,`canEarlyHelp`,`canWork`,`didPurser`,`had_study_other`,`study_other`,`had_teach`,`teach`,`had_physiclal`,`physiclal`,`had_mental`,`mental`,`had_medicine`,`medicine`,`had_drug`,`drug`,`first_study_date_from`,`first_study_school`,`first_study_teacher`,`last_study_date_from`,`last_study_school`,`last_study_teacher`,`study_count_10day`,`study_count_SatipatthanaSutta`,`study_count_20day`,`study_count_30day`,`study_count_45day`,`service_count_10day`,`service_count_SatipatthanaSutta`,`service_count_20day`,`service_count_30day`,`service_count_45day`,`heath_had_bloodPressure`,`heath_bloodPressure`,`heath_had_diabetes`,`heath_diabetes`,`heath_had_heart`,`heath_heart`,`heath_had_mental`,`heath_mental`,`heath_had_drug`,`heath_drug`,`heath_had_hallucination`,`heath_hallucination`,`heath_had_otherDisease`,`heath_otherDisease`,`heath_had_pregnancy`,`heath_pregnancy`,`heath_had_specialArrangements`,`heath_specialArrangements`,`worker_work`,`worker_schools`,`enroll_status`,`check_status`,`creator`,`create_time`,`last_updator`,`last_update_time`,`enroll_time`) VALUES 
		(8+i,5,22,'NM','REGULAR','安徽省滁州市','STUDENT','MALE',concat('name', i),'','二代身份证',concat('id123', i),'15895845287',concat(concat('mail', i),'@aa.com'),'',concat(concat('mail2', i),'@aa.com'),NULL,'路14-16','安徽省滁州市','233100','中国','1995-08-31','男在家众',NULL,'计算机/互联网/通信/电子','python开发','本科','','',1,0,0,0,'0',0,'','','互联网','AAA',NULL,'母子',1,0,NULL,0,0,'0',1,'学习过观呼吸',0,'',0,'',1,'一两年前有些焦躁恐慌，目前心理基本没问题。',0,'',0,'',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',0,'',NULL,NULL,'VALID','CHECK_WAITING','测试',now(),'测试',now(),now());
		
		set i=i+1;
	end while;

	INSERT INTO `bad_enroll` (`bad_enroll_id`,`course_id`,`enroll_id`,`schoolId`,`school_name`,`school_country`,`course_date_from`,`course_date_to`,`course_type_name`,`course_days`,`course_class`,`teacher`,`cn_name`,`en_name`,`id_type`,`id_num`,`gender`,`mobile`,`email`,`birthday`,`age`,`country`,`isOld`,`release_date`,`remark`,`creator`,`create_time`,`last_updator`,`last_update_time`) VALUES 
    (183,NULL,NULL,3,'青岛内观中心','CN','2017-10-11','2017-10-22','十日课程',NULL,NULL,'陈育光，穆丽娟',v_male_name,'','二代身份证',v_male_idnum,'FEMALE','15263700808','577214677@qq.com','1990-06-21',27,'中国',0,'2018-04-11',NULL,'旧系统','2017-10-22 00:00:00','旧系统数据导入程序','2017-11-03 23:56:24');

	INSERT INTO `course` (`course_id`,`course_type_id`,`school_id`,`enroll_date_from`,`enroll_date_to`,`enroll_confirm_date_from`,`enroll_confirm_date_to`,`arrival_confirm_date_from`,`arrival_confirm_date_to`,`male_new`,`male_new_spare`,`male_old`,`male_old_spare`,`female_new`,`female_new_spare`,`female_old`,`female_old_spare`,`male_worker`,`female_worker`,`course_code`,`course_desp`,`remark`,`is_enroll_stoped`,`course_date_from`,`course_date_to`,`language`,`teachers`,`admin`,`status`,`cancelcause`) VALUES 
	(371,2,1,'2011-07-27','2011-09-18','2011-09-06','2011-09-08',NULL,NULL,1,0,1,1,1,0,1,0,0,15,'','','',1,'2011-10-01','2011-10-12',NULL,'陈跃章/王慧 老师','萧集智 老师','FINISHED',NULL)
	, (484,2,1,'2012-10-27','2012-12-28','2012-12-19','2012-12-20',NULL,NULL,1,1,40,30,0,0,0,0,15,3,'','男众课程，2012-10-27受理报名。','',1,'2013-01-01','2013-01-12',NULL,'李国安老师','萧集智老师','FINISHED',NULL)
	, (559,2,1,'2013-11-19','2013-12-18','2013-12-19','2013-12-20',NULL,NULL,30,25,20,15,38,30,26,20,10,10,'','男女众课程，11月19日开始接受报名','',1,'2014-01-01','2014-01-12',NULL,'林秀蓉','萧集智','FINISHED',NULL)
	, (729,2,1,'2015-05-21','2015-07-22','2015-07-23','2015-07-24',NULL,NULL,30,25,20,15,38,30,26,20,12,17,'','5月21日开始接受报名','',1,'2015-08-05','2015-08-16',NULL,'李锦芳，唐贤君','方巧玲 廖燕','FINISHED',NULL)
	, (941,1,1,'2017-01-02','2017-06-30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,3,'','不接受电话报名和在线申请，需发电子邮件到中心邮箱索取特别报名表和助理老师推荐 报名资格介绍 http://vipassana.sutta.org/vipassana-specialcource.htm','',1,'2017-07-19','2017-08-09',NULL,'萧集智，戴明娇','方巧玲，廖燕','FINISHED',NULL)
	, (881,2,1,'2016-05-16','2016-06-24','2016-06-16','2016-06-17',NULL,NULL,30,20,20,10,38,20,26,15,10,10,'','5月16号开始报名','',1,'2016-06-29','2016-07-10',NULL,'萧集智，萧若馨','方巧玲，廖燕','FINISHED',NULL);

	INSERT INTO `enroll` (`enroll_id`,`course_id`,`recommendation`,`enroll_time`,`student_type`,`gender`,`cn_name`,`en_name`,`id_type`,`id_num`,`idcity`,`mobile`,`email`,`mobile2`,`email2`,`photo`,`address`,`city`,`postcode`,`country`,`birthday`,`age`,`religion`,`religion_date`,`profession`,`occupation`,`education`,`parent`,`parent_phone`,`canEnglish`,`canChinese`,`canTaiyu`,`canYueyu`,`otherlanguages`,`has_together`,`together`,`together_relationship`,`howKnown`,`had_study_other`,`study_other`,`had_teach`,`teach`,`had_physiclal`,`physiclal`,`had_mental`,`mental`,`had_medicine`,`medicine`,`had_drug`,`drug`,`isOld`,`hasExercise`,`hours_per_week`,`canEarlyHelp`,`canWork`,`didPurser`,`first_study_date_from`,`first_study_school`,`first_study_teacher`,`last_study_date_from`,`last_study_school`,`last_study_teacher`,`study_count_10day`,`study_count_SatipatthanaSutta`,`study_count_20day`,`study_count_30day`,`study_count_45day`,`service_count_10day`,`service_count_SatipatthanaSutta`,`service_count_20day`,`service_count_30day`,`service_count_45day`,`heath_had_bloodPressure`,`heath_bloodPressure`,`heath_had_diabetes`,`heath_diabetes`,`heath_had_heart`,`heath_heart`,`heath_had_mental`,`heath_mental`,`heath_had_drug`,`heath_drug`,`heath_had_hallucination`,`heath_hallucination`,`heath_had_otherDisease`,`heath_otherDisease`,`heath_had_pregnancy`,`heath_pregnancy`,`heath_had_specialArrangements`,`heath_specialArrangements`,`linealKin`,`linealKin_phone`,`linealKin_relationship`,`studentOrWorker`,`worker_work`,`worker_schools`,`full_time`,`to_reach_date`,`to_leave_date`,`commitment_image`,`healthDetail_image`,`linealKin_id_image_front`,`linealKin_id_image_back`,`linealKin_video`,`need_additional`,`first_confirm_time`,`first_confirm_user`,`first_confirm_remark`,`second_confirm_time`,`second_confirm_user`,`second_confirm_remark`,`second_confirm_arrivaltime`,`checkin_remark`,`enroll_status`,`course_finished`,`undone_course_reason`,`remark`,`check_status`,`check_remark`,`applicant_operations`,`check_user_name`,`check_time`,`creator`,`create_time`,`last_updator`,`last_update_time`) VALUES 
	(25376,371,NULL,'2011-07-27 00:00:00','NM','MALE','李四','','二代身份证','440106198202020555','广东省广州市','13316116676','yuz2@163.com','',NULL,'','广州白云区时代玫瑰园三期','广东省广州市','510440','中国','1982-02-02',40,'男在家众',NULL,NULL,'IT','本科',NULL,NULL,0,0,0,0,'0',0,'','','妻子',0,'',0,'',0,'',0,'',0,'',0,'',0,0,NULL,0,0,'0',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,NULL,NULL,NULL,'STUDENT',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VALID',1,NULL,'','REGULAR','',NULL,NULL,NULL,'旧系统','2011-07-27 00:00:00','旧系统数据导入程序','2017-11-06 21:13:05')
	, (38840,484,'REGULAR','2012-11-28 10:09:01','OM','MALE','李四','','二代身份证','440106198202020555','广东省广州市','13316116676','yuz2@163.com','',NULL,'','广州某某区某街','广东省广州市','510000','中国 ','1982-02-02',41,'男在家众',NULL,NULL,'IT','本科','','',0,1,0,0,'0',0,'','',NULL,0,'',0,'',0,'',0,'',0,'',0,'',1,1,'5',0,0,'0','2011-10-01','福建南禅寺','陈老师','2011-10-01','福建南禅寺','陈老师',1,0,0,0,0,0,0,0,0,0,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,NULL,NULL,NULL,'STUDENT',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VALID',1,NULL,'','REGULAR','',NULL,NULL,NULL,'旧系统','2012-11-28 10:09:01','旧系统数据导入程序','2017-11-06 21:13:15')
	, (50273,559,'REGULAR','2013-11-19 09:05:03','OM','MALE','李四','','二代身份证','440106198202020555','广东省广州市','13316116676','yud7@163.com','',NULL,'','广州某某区某街','广东省广州市','510000','中国','1982-02-02',42,'男在家众',NULL,NULL,'IT','本科','','',0,1,0,0,'0',0,'','',NULL,0,'',0,'',0,'',0,'',0,'',0,'',1,1,'7',0,0,'0','2011-10-01','福建南禅寺','陈老师','2013-01-01','福建南禅寺','李老师',2,0,0,0,0,0,0,0,0,0,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,NULL,NULL,NULL,'STUDENT',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VALID',1,NULL,'','REGULAR','',NULL,NULL,NULL,'旧系统','2013-11-19 09:05:03','旧系统数据导入程序','2017-11-06 21:13:25')
	, (79089,729,'BACKUP','2015-07-23 11:32:25','OM','MALE','李四','','二代身份证','440106198202020555','广东省广州市','13316116676','543043762@qq.com','',NULL,'','某某区某街','广东省广州市','510720','中国','1982-02-02',44,'男在家众',NULL,NULL,'IT','本科','','',0,0,0,0,'0',0,'','',NULL,0,'',0,'',0,'',0,'',0,'',0,'',1,1,'7',0,1,'0','2011-10-01','福建南禅寺','陈老师','2014-07-16','丹东双灵寺','萧老师',3,1,0,0,0,0,0,0,0,0,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,NULL,NULL,NULL,'STUDENT',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VALID',1,NULL,'用户查询后，重新申请参加此次课程成功。','REGULAR','',NULL,NULL,NULL,'旧系统','2015-07-23 11:32:25','旧系统数据导入程序','2017-11-06 21:13:52')
	, (119807,941,'BACKUP','2017-03-21 16:35:59','OM','MALE','李四','','二代身份证','440106198202020555','广东省广州市','13316116676','543043762@qq.com','',NULL,'','某某区某街21-1号','广东省广州市','510000','中国','1982-02-02',45,'男在家众',NULL,'计算机/互联网/通信/电子','经理','本科','','',1,1,0,0,'0',0,'','',NULL,0,'',0,'',0,'',0,'',0,'',0,'',1,1,'14',0,0,'0','2011-10-01','南禅寺','陈跃章','2016-10-01','厦门真寂寺','李国安老师',5,1,0,0,0,1,0,0,0,0,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,NULL,NULL,NULL,'STUDENT',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VALID',1,'','','REGULAR','',NULL,NULL,NULL,'旧系统','2017-03-21 16:35:59','旧系统数据导入程序','2017-11-06 21:14:38')
	, (196591,881,NULL,'2016-06-17 00:00:00',NULL,'MALE','李四','','二代身份证','440106198202020555','广东省','13316116676','543043762@qq.com','',NULL,NULL,'某某区某街21-1号','广东省','510720','中国','1982-02-02',45,'男在家众',NULL,'计算机/互联网/通信/电子','IT','本科','','',0,1,0,0,'0',0,'','',NULL,0,'',0,'',0,'',0,'',0,'',0,'',1,1,'14',0,0,'0','2011-10-01','福建南禅寺','陈跃章老师','2015-08-05','福建南禅寺','李锦芳老师',4,1,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'WORKER','报名组',NULL,1,'','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VALID',1,NULL,NULL,'REGULAR','',NULL,NULL,NULL,'旧系统','2016-06-17 00:00:00','旧系统数据导入程序','2017-11-06 21:17:09')
	, (38841,484,'REGULAR','2012-11-28 10:09:01','NF','FEMALE','钱钱','','二代身份证','110104198807065167','广东省广州市','13311111111','dd@163.com','',NULL,'','广州某某区某街','广东省广州市','510000','中国 ','1988-07-06',24,'女在家众',NULL,NULL,'IT','本科','','',0,0,0,0,'0',0,'','',NULL,0,'',0,'',0,'',0,'',0,'',0,'',0,0,NULL,0,0,'0',NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,0,NULL,NULL,NULL,NULL,'STUDENT',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VALID',1,NULL,'','REGULAR','',NULL,NULL,NULL,'旧系统','2012-11-28 10:09:01','旧系统数据导入程序','2017-11-06 21:13:15')
    ;

	INSERT INTO `study_work_record` (`study_work_record_id`,`course_id`,`cn_name`,`en_name`,`id_type`,`id_num`,`gender`,`mobile`,`email`,`birthday`,`age`,`country`,`schoolId`,`school_name`,`school_country`,`course_date_from`,`course_date_to`,`course_type_name`,`course_days`,`course_class`,`teacher`,`course_finished`,`leavetime`,`leavecause`,`remark`,`studentOrWorker`,`work`,`full_time`,`date_from`,`date_to`,`creator`,`create_time`,`last_updator`,`last_update_time`) VALUES 
	(18648,371,'李四','','二代身份证','440106198202020555','MALE','13316116676','yuz2@163.com','1982-02-02',40,'中国',1,'南禅寺内观中心','CN','2011-10-01','2011-10-12','十日课程',10,'COMMONDAYS','陈跃章/王慧 老师',1,NULL,NULL,NULL,'STUDENT',NULL,1,NULL,NULL,'旧系统','2011-10-12 00:00:00','旧系统数据导入程序','2017-11-06 21:17:51')
	, (23697,484,'李四','','二代身份证','440106198202020555','MALE','13316116676','yuz2@163.com','1982-02-02',41,'中国 ',1,'南禅寺内观中心','CN','2013-01-01','2013-01-12','十日课程',10,'COMMONDAYS','李国安老师',1,NULL,NULL,NULL,'STUDENT',NULL,1,NULL,NULL,'旧系统','2013-01-12 00:00:00','旧系统数据导入程序','2017-11-06 21:17:55')
	, (29359,559,'李四','','二代身份证','440106198202020555','MALE','13316116676','yud7@163.com','1982-02-02',42,'中国',1,'南禅寺内观中心','CN','2014-01-01','2014-01-12','十日课程',10,'COMMONDAYS','林秀蓉',1,NULL,NULL,NULL,'STUDENT',NULL,1,NULL,NULL,'旧系统','2014-01-12 00:00:00','旧系统数据导入程序','2017-11-06 21:18:00')
	, (41833,729,'李四','','二代身份证','440106198202020555','MALE','13316116676','543043762@qq.com','1982-02-02',44,'中国',1,'南禅寺内观中心','CN','2015-08-05','2015-08-16','十日课程',10,'COMMONDAYS','李锦芳，唐贤君',1,NULL,NULL,NULL,'STUDENT',NULL,1,NULL,NULL,'旧系统','2015-08-16 00:00:00','旧系统数据导入程序','2017-11-06 21:18:13')
	, (65005,941,'李四','','二代身份证','440106198202020555','MALE','13316116676','543043762@qq.com','1982-02-02',45,'中国',1,'南禅寺内观中心','CN','2017-07-19','2017-08-09','二十日课程',20,'LONGDAYS','萧集智，戴明娇',1,NULL,NULL,NULL,'STUDENT',NULL,1,NULL,NULL,'旧系统','2017-08-09 00:00:00','旧系统数据导入程序','2017-11-06 21:18:36')
	, (74014,881,'李四','','二代身份证','440106198202020555','MALE','13316116676','543043762@qq.com','1982-02-02',45,'中国',1,'南禅寺内观中心','CN','2016-06-29','2016-07-10','十日课程',10,'COMMONDAYS','萧集智，萧若馨',1,NULL,NULL,'','WORKER','报名组',1,NULL,NULL,'旧系统','2016-06-17 00:00:00','旧系统数据导入程序','2017-11-06 21:18:51')
	, (74015,484,'钱钱','','二代身份证','110104198807065167','FEMALE','13311111111','dd@163.com','1988-07-06',24,'中国 ',1,'南禅寺内观中心','CN','2013-01-01','2013-01-12','十日课程',10,'COMMONDAYS','李国安老师',1,NULL,NULL,NULL,'STUDENT',NULL,1,NULL,NULL,'旧系统','2013-01-12 00:00:00','旧系统数据导入程序','2017-11-06 21:17:55')
    ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_test_initCourses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ng_test_initCourses`()
BEGIN
	DECLARE v_today Date;
	DECLARE v_schoolId bigint;
	DECLARE v_courseId bigint;
	DECLARE v_course_type_id_10days bigint;
	DECLARE v_course_interval int;
    
    set v_today = CURDATE();
    set v_schoolId=1;
    set v_courseId=3;
    set v_course_type_id_10days=2;
    set v_course_interval=4;
    
    

	
    call ng_test_insertCourse(v_schoolId, v_courseId-2, v_course_type_id_10days, date_add(v_today, interval -40 day),20,10,5,30,16,8,null);
    call ng_test_insertCourse(v_schoolId, v_courseId-1, v_course_type_id_10days, date_add(v_today, interval -25 day),20,10,5,30,16,8,null);

	
    call ng_test_insertCourse(v_schoolId, v_courseId, v_course_type_id_10days, date_add(v_today, interval -10 day),20,10,5,30,16,8,null);

	
    call ng_test_insertCourse(v_schoolId, v_courseId+1, v_course_type_id_10days, date_add(v_today, interval 5 day),20,10,5,30,16,8,null);
	
    call ng_test_insertCourse(v_schoolId, v_courseId+2, v_course_type_id_10days, date_add(v_today, interval 20 day),20,10,5,30,16,8,v_today);

	
    call ng_test_insertCourse(v_schoolId, v_courseId+3, v_course_type_id_10days, date_add(v_today, interval 35 day),0,0,0,30,16,8,null);
    
    
    call ng_test_insertCourse(v_schoolId, v_courseId+4, v_course_type_id_10days, date_add(v_today, interval 50 day),20,10,5,30,16,8,null);
    call ng_test_insertCourse(v_schoolId, v_courseId+5, v_course_type_id_10days, date_add(v_today, interval 65 day),20,10,5,30,16,8,null);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_test_insertCourse` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ng_test_insertCourse`(
	v_schoolId bigint,
	v_courseId bigint,
	v_course_type_id bigint,
	v_dateFrom Date,
    v_male_new int,
    v_male_old int,
    v_male_worker int,
    v_female_new int,
    v_female_old int,
    v_female_worker int,
    v_first_confirm Date
)
BEGIN
	DECLARE v_courseDays int;
    
    SELECT `course_days` into v_courseDays FROM `course_type` where `course_type_id`=v_course_type_id;
	set v_courseDays=v_courseDays+1;

	INSERT INTO `course`
		(`course_id`,`course_type_id`,`school_id`,
        `course_date_from`,`course_date_to`,
        `teachers`,`language`,`status`,
		`is_enroll_stoped`,	`enroll_date_from`,	`enroll_date_to`,
        `enroll_confirm_date_from`,`enroll_confirm_date_to`,
		`arrival_confirm_date_from`,`arrival_confirm_date_to`,
		`male_new`,`male_new_spare`,
		`male_old`,`male_old_spare`,
		`female_new`,`female_new_spare`,
		`female_old`,`female_old_spare`,
		`male_worker`,`female_worker`)
		VALUES (
		v_courseId,	v_course_type_id, v_schoolId, 
		v_dateFrom, date_add(v_dateFrom, interval v_courseDays day),
		'',	'普通话', 'STANDBY',
		0, date_add(v_dateFrom, interval -49 day), date_add(v_dateFrom, interval -7 day),
		ifnull(v_first_confirm, date_add(v_dateFrom, interval -14 day)), date_add(ifnull(v_first_confirm, date_add(v_dateFrom, interval -14 day)), interval +2 day),
		date_add(v_dateFrom, interval -5 day), date_add(v_dateFrom, interval -4 day),
		v_male_new, v_male_new/2,
		v_male_old, v_male_old/2,
		v_female_new, v_female_new/2,
		v_female_old, v_female_old/2,
		v_male_worker,v_female_worker
		);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ng_user_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `ng_user_delete`(v_userId bigint)
BEGIN

DELETE FROM `sys_user_role` WHERE user_id=v_userId;

DELETE FROM `sys_limit_authority` WHERE user_authority_limit_id in (
	select user_authority_limit_id from sys_user_authority_limit WHERE user_id=v_userId
);

DELETE FROM `sys_limit_course_type` WHERE user_authority_limit_id in (
	select user_authority_limit_id from sys_user_authority_limit WHERE user_id=v_userId
);

DELETE FROM `sys_limit_school` WHERE user_authority_limit_id in (
	select user_authority_limit_id from sys_user_authority_limit WHERE user_id=v_userId
);

DELETE FROM `sys_limit_tag` WHERE user_authority_limit_id in (
	select user_authority_limit_id from sys_user_authority_limit WHERE user_id=v_userId
);

DELETE from sys_user_authority_limit WHERE user_id=v_userId;

DELETE from sys_user WHERE user_id=v_userId;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-17 22:36:58
