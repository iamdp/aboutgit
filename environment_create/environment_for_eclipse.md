﻿# eclipse环境搭建：
### eclipse下载

[下载eclipse for windows](https://www.eclipse.org/downloads/eclipse-packages/)

###### 选择如图版本：

![选择如图版本](eclipse_download.png "选择如图版本")

###### 下载完之后：

------------

- 解压之后在双击打开

![打开eclipse](eclipse_open.png "打开eclipse")

------------



- 好看的界面

![eclipse_view](eclipse_view.png "eclipse_view")

------------

- 选择workspace目录，就是你的项目存储路径

![eclipse_workspace_choose](eclipse_workspace_choose.png "eclipse_workspace_choose")

------------



### 打开window->>preference来配置一下基本开发环境 


![配置页面](config_view.png "config_view")

+ **配置字体大小**

  ![字体配置](font_seting.png "font_seting") 


+ **配置编码方式**

  ![编码配置](coding_setting.png "codeing_setting") 

+ **配置jre**
     
     > **路径**

    ![路径配置](jre_setting.png "jre_setting") 

     > **选择生效**
    
      ![路径配置](jre_setting1.png "jre_setting1")  

+ **配置maven**
     
     > 安装maven，解压到某个路径

    ![解压](maven_setting4.png "解压maven") 

    
    >**设置环境变量**
   

    ![设置环境变量](maven_setting5.png "设置环境变量1") 
     ------------------------------------------------------------------------
     ------------------------------------------------------------------------
    ![设置环境变量](maven_setting6.png "设置环境变量2") 

      >**将eclipse中maven配置成本地maven**


      ![local_maven](maven_setting2.png "local_maven1")


      ![local_maven](maven_setting3.png "local_maven2") 


      ![local_maven](maven_setting.png "local_maven3")


      ![local_maven](maven_setting1.png "local_maven4") 

+ **配置Git**

    >**eclipse中的git插件**

    ![eclipse_git](git_setting.png "eclipse_git_plugin")



     >**在eclipse中配置git命令行**



     ![git_console](git_console.png "git_console1")



     ![git_console](git_console2.png "git_console2")


     ![git_console](git_console3.png "git_console3")


     ![git_console](git_console4.png "git_console4")


     ![git_console](git_console5.png "git_console5")


    ## eclipse配置结束
     ------------------------------------------

     --------------------------------

    ## 运行项目步骤

     > 将sdk用maven打包
     
     ![mvn_install](ng_sdk_install1.png "ng_sdk_install1")


     ![mvn_install](ng_sdk_install2.png "ng_sdk_install2")


    ![mvn_install](ng_sdk_install3.png "ng_sdk_install3")



    >导入项目,先将项目拷贝到workspace路径然后在eclipse中选择File-->import


    ![import1](import_1.png "import1")

    ![import2](import_2.png "import2")

    ![import3](import_4.png "import3")

    >运行项目


    ![run_1](run_1.png "run1")

    ![run_2](run_2.png "run2")

    ![run_3](run_4.png "run3")

      >在eclipse中使用git命令行

      ![git_console](git_console5.png "git_console5")


      ![git_console](git_console6.png "git_console6")


      ![git_console](git_console7.png "git_console7")


        






