# 系统部署安装指南

## 测试环境系统部署

### 系统关系图

![系统关系图](systems-ref.png)

#### 需要的外部组件
 * mysql
 * redis

#### 系统依赖关系

系统 | 简介 | 直接调用的外部系统 | 间接依赖的系统
-|-|-|-
ngschoolsite | 学校端 | ngschoolservice-server | ngtaskexecutor
ngschoolservice-server | 学校API服务端 | mysql、redis | ngtaskexecutor
ngtaskexecutor | 耗时任务处理端 | mysql、redis | 
ngworkwebsite | 管理端 | mysql、redis | ngtaskexecutor

### 建议的部署方案
 1. 在虚拟机中安装mysql、redis，推荐使用centos7

数据库脚本：[schama：new_neiguan_schema.sql](new_neiguan_schema.sql)，[data：new_neiguan_data.sql](new_neiguan_data.sql)

```sql
create database new_neiguan character set utf8 collate utf8_general_ci;
use new_neiguan;
source new_neiguan_schema.sql;
source new_neiguan_data.sql;
call ng_test_init(); 
``` 

存储过程ng_test_init：初始化测试数据，根据当前时间创建课程。如果发现数据库里的课程都过期了，可以调用这个存储过程，重新构建课程。注意：会清空school、course、enroll等表。

具体安装过程，参见：[centos-install.md](centos-install.md)

 2. 在本机运行某个项目

 ngworkwebsite登录: 使用 admin@vipdha.com 邮箱登录，不需点击“获取验证码”，直接在验证码输入框填入任意字符，就可以登录

 ngschoolsite旧生入口登录，需要使用enroll表中已经存在的邮箱登录，获取验证码，使用邮箱收到的验证码登录才可以进入旧生首页。因此，请先使用自己的邮箱报名某一期课程，再登录


